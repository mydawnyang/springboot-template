# springboot-template

#### 介绍
基于SpringBoot构建的快速开发脚手架，默认集成了阿里OSS对象存储、Mail邮件自动发送、Redis各个数据类型操作封装、JWTToken认证、MyBatisPlus基础单表操作、基础文件生成等功能

#### 安装教程

1.  修改`application-*.properties` 的MySQL数据库和Redis以及阿里OSS相关配置，改成自己的配置
    MYSQL密码需要加密调用 `JasyptUtil.encryptPwd(公钥,密码)` 然后把公钥填入`jasypt.encryptor.password=公钥`，加密后的密码填入`spring.datasource.password=ENC(加密后的密码)`
2.  如果需要使用代码生成器 需要调整`AutoGeneratorUtil`内的数据库连接，代码生成路径
3.  数据库结构文件在doc内的template.sql内

#### 软件架构
框架依赖：
```xml

	<properties>
		<java.version>1.8</java.version>
		<springboot.version>2.2.1.RELEASE</springboot.version>
		<lombok.version>1.18.8</lombok.version>
		<mybatis-plus-boot-starter.version>3.2.0</mybatis-plus-boot-starter.version>
		<mybatis-plus.version>3.2.0</mybatis-plus.version>
		<mysql-connector.version>8.0.16</mysql-connector.version>
		<druid-spring-boot-starter.version>1.1.20</druid-spring-boot-starter.version>
		<freemarker.version>2.3.28</freemarker.version>
		<auth0-java-jwt.version>3.2.0</auth0-java-jwt.version>
		<jsonwebtoken-jwt.version>0.7.0</jsonwebtoken-jwt.version>
		<fastjson.version>1.2.58</fastjson.version>
		<commons-lang3.version>3.9</commons-lang3.version>
		<aliyun-oss-sdk.version>3.8.0</aliyun-oss-sdk.version>
		<jasypt-spring-boot-starter.version>2.1.2</jasypt-spring-boot-starter.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<!-- Spring切面AOP相关依赖 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-aop</artifactId>
			<version>${springboot.version}</version>
		</dependency>
		<!-- 整合Redis -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-redis</artifactId>
			<version>${springboot.version}</version>
		</dependency>
		<!-- 整合Mail发送功能 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-mail</artifactId>
			<version>${springboot.version}</version>
		</dependency>
		<!--文本加解密-->
		<dependency>
			<groupId>com.github.ulisesbocchio</groupId>
			<artifactId>jasypt-spring-boot-starter</artifactId>
			<version>${jasypt-spring-boot-starter.version}</version>
		</dependency>
		<!-- SpringBoot自动装填配置依赖 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<version>${springboot.version}</version>
			<optional>true</optional>
		</dependency>
		<!-- 模板框架 -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>${lombok.version}</version>
		</dependency>
		<!-- MySQL数据库连接驱动 -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>${mysql-connector.version}</version>
		</dependency>
		<!-- MybatisPlus Mybatis增强插件SpringBoot整合 -->
		<dependency>
			<groupId>com.baomidou</groupId>
			<artifactId>mybatis-plus-boot-starter</artifactId>
			<version>${mybatis-plus-boot-starter.version}</version>
		</dependency>
		<!-- Druid阿里数据库连接池 -->
		<dependency>
			<groupId>com.alibaba</groupId>
			<artifactId>druid-spring-boot-starter</artifactId>
			<version>${druid-spring-boot-starter.version}</version>
		</dependency>
		<!-- 代码生成器插件 -->
		<dependency>
			<groupId>com.baomidou</groupId>
			<artifactId>mybatis-plus-generator</artifactId>
			<version>${mybatis-plus.version}</version>
		</dependency>
		<!-- Freemarker引擎 代码生成使用 -->
		<dependency>
			<groupId>org.freemarker</groupId>
			<artifactId>freemarker</artifactId>
			<version>${freemarker.version}</version>
		</dependency>
		<!-- JWT 签名依赖 -->
		<dependency>
			<groupId>com.auth0</groupId>
			<artifactId>java-jwt</artifactId>
			<version>${auth0-java-jwt.version}</version>
		</dependency>
		<dependency>
			<groupId>io.jsonwebtoken</groupId>
			<artifactId>jjwt</artifactId>
			<version>${jsonwebtoken-jwt.version}</version>
		</dependency>
		<!-- 阿里的FastJson -->
		<dependency>
			<groupId>com.alibaba</groupId>
			<artifactId>fastjson</artifactId>
			<version>${fastjson.version}</version>
		</dependency>
		<!-- apache的处理基本数据类型数据的工具包 -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>${commons-lang3.version}</version>
		</dependency>
		<!-- 阿里云OSS-SDK -->
		<dependency>
			<groupId>com.aliyun.oss</groupId>
			<artifactId>aliyun-sdk-oss</artifactId>
			<version>${aliyun-oss-sdk.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.junit.vintage</groupId>
					<artifactId>junit-vintage-engine</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
	</dependencies>

```


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
