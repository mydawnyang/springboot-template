/*
 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001
 Date: 13/03/2020 12:44:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sm_data_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `sm_data_dictionary`;
CREATE TABLE `sm_data_dictionary` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父ID',
  `data_title` varchar(4000) DEFAULT NULL COMMENT '数据标题',
  `data_value` varchar(4000) DEFAULT NULL COMMENT '数据值',
  `order_no` varchar(4000) DEFAULT NULL COMMENT '排序编号',
  `remark` varchar(4000) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统数据字典表';

-- ----------------------------
-- Table structure for sm_file
-- ----------------------------
DROP TABLE IF EXISTS `sm_file`;
CREATE TABLE `sm_file` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `old_file_name` varchar(30) DEFAULT NULL COMMENT '原文件名',
  `file_name` varchar(30) DEFAULT NULL COMMENT '文件名',
  `file_hash` varchar(256) DEFAULT NULL COMMENT 'HASH唯一标识',
  `file_format` varchar(20) DEFAULT NULL COMMENT '文件格式 （后缀）',
  `file_size` int(11) DEFAULT NULL COMMENT '文件大小 （字节单位）',
  `folder_id` varchar(32) DEFAULT NULL COMMENT '所属文件夹',
  `logical_path` varchar(200) DEFAULT NULL COMMENT '逻辑路径',
  `physics_path` varchar(200) DEFAULT NULL COMMENT '物理路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统文件表';

-- ----------------------------
-- Table structure for sm_folder
-- ----------------------------
DROP TABLE IF EXISTS `sm_folder`;
CREATE TABLE `sm_folder` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `folder_name` varchar(120) DEFAULT NULL COMMENT '文件夹名称',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父级ID',
  `folder_path` varchar(120) DEFAULT NULL COMMENT '逻辑路径',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人员',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `folder_type` tinyint(4) DEFAULT NULL COMMENT '文件夹类型 （0：系统文件夹；1：用户文件夹）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统文件夹表';

-- ----------------------------
-- Table structure for sm_mail_sender
-- ----------------------------
DROP TABLE IF EXISTS `sm_mail_sender`;
CREATE TABLE `sm_mail_sender` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `mail_host` varchar(20) NOT NULL COMMENT '邮件服务方地址',
  `mail_port` varchar(6) NOT NULL COMMENT '邮件发送端口',
  `mail_form` varchar(20) DEFAULT NULL COMMENT '发送方昵称',
  `user_name` varchar(30) NOT NULL COMMENT '邮件发送方用户名-邮箱地址',
  `pass_word` varchar(20) NOT NULL COMMENT '第三方授权码或者密码',
  `default_encoding` varchar(9) DEFAULT 'UTF-8' COMMENT '默认邮件编码',
  `name` varchar(20) DEFAULT NULL COMMENT '真实名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮件发送方表';

-- ----------------------------
-- Table structure for sm_organization
-- ----------------------------
DROP TABLE IF EXISTS `sm_organization`;
CREATE TABLE `sm_organization` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `organization_name` varchar(80) DEFAULT NULL COMMENT '组织名称',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父级ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人员',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统组织表';

-- ----------------------------
-- Table structure for sm_resource
-- ----------------------------
DROP TABLE IF EXISTS `sm_resource`;
CREATE TABLE `sm_resource` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `res_name` varchar(50) DEFAULT NULL COMMENT '资源名称',
  `res_route` varchar(120) DEFAULT NULL COMMENT '资源路由',
  `res_logo` varchar(32) DEFAULT NULL COMMENT '资源LOGO',
  `res_id` varchar(20) DEFAULT NULL COMMENT '资源标识',
  `res_type` tinyint(4) DEFAULT '0' COMMENT '资源类型 （0：菜单；1：按钮）',
  `res_status` tinyint(4) DEFAULT '0' COMMENT '资源状态 （0：正常；1：失效）',
  `remark` varchar(500) DEFAULT NULL COMMENT '资源备注',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父级ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统资源表';

-- ----------------------------
-- Table structure for sm_role
-- ----------------------------
DROP TABLE IF EXISTS `sm_role`;
CREATE TABLE `sm_role` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `role_name` varchar(20) DEFAULT NULL COMMENT '角色名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人员',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDU_sm_role_role_name` (`role_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Table structure for sm_role_resource_relation
-- ----------------------------
DROP TABLE IF EXISTS `sm_role_resource_relation`;
CREATE TABLE `sm_role_resource_relation` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `role_id` varchar(32) DEFAULT NULL COMMENT '角色ID',
  `resource_id` varchar(32) DEFAULT NULL COMMENT '资源ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统角色资源关系表';

-- ----------------------------
-- Table structure for sm_user
-- ----------------------------
DROP TABLE IF EXISTS `sm_user`;
CREATE TABLE `sm_user` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `user_photo` varchar(32) DEFAULT NULL COMMENT '用户头像',
  `user_name` varchar(32) DEFAULT NULL COMMENT '用户名称',
  `user_phone` varchar(11) NOT NULL COMMENT '手机号码',
  `login_pass` varchar(64) DEFAULT NULL COMMENT '登录密码',
  `user_email` varchar(80) DEFAULT NULL COMMENT '电子邮箱',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `user_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户状态 （0：正常；1：失效）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `INDEX-sm_user-user_phone-UNIQUE` (`user_phone`) USING BTREE COMMENT '用户手机号必须是唯一的'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- ----------------------------
-- Table structure for sm_user_label
-- ----------------------------
DROP TABLE IF EXISTS `sm_user_label`;
CREATE TABLE `sm_user_label` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `label_title` varchar(20) DEFAULT NULL COMMENT '标签标题',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人员',
  `remark` varchar(4000) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户标签表';

-- ----------------------------
-- Table structure for sm_user_label_relation
-- ----------------------------
DROP TABLE IF EXISTS `sm_user_label_relation`;
CREATE TABLE `sm_user_label_relation` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `label_id` varchar(32) DEFAULT NULL COMMENT '标签ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户标签关系表';

-- ----------------------------
-- Table structure for sm_user_role_relation
-- ----------------------------
DROP TABLE IF EXISTS `sm_user_role_relation`;
CREATE TABLE `sm_user_role_relation` (
  `id` varchar(32) NOT NULL COMMENT 'PK',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `role_id` varchar(32) DEFAULT NULL COMMENT '角色ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人员',
  PRIMARY KEY (`id`),
  KEY `IDX_sm_userion_user_id154E` (`user_id`),
  KEY `IDX_sm_userion_role_idB867` (`role_id`),
  KEY `IDX_sm_userion_create_ser0026` (`create_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户角色关系表';

SET FOREIGN_KEY_CHECKS = 1;
