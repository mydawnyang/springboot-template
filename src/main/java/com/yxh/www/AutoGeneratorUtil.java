package com.yxh.www;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;

/**
 * 代码生成器
 * @author yangxiaohui
 */
public class AutoGeneratorUtil {
    private static final String projectPath = "D:\\templateProject\\template";

    public static void main(String[] args) {
        AutoGenerator autoGenerator = new AutoGenerator();
        // 全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setOutputDir(projectPath + "/src/main/java");
        globalConfig.setAuthor("Yangxiaohui");
        globalConfig.setSwagger2(false);
        globalConfig.setActiveRecord(true);
        globalConfig.setOpen(false);
        globalConfig.setBaseResultMap(true);
        //globalConfig.setEntityName("");
        globalConfig.setMapperName("%sMapper");
        globalConfig.setServiceName("%sService");
        globalConfig.setServiceImplName("%sServiceImpl");
        globalConfig.setControllerName("%sController");
        // 设置覆盖已有文件
        globalConfig.setFileOverride(false);
        globalConfig.setIdType(IdType.UUID);

        // 数据源配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setUrl("jdbc:mysql://数据库地址:端口/数据库?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=CTT&unullNamePatternMatchesAll=true&autoReconnect=true");
        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
        dataSourceConfig.setUsername("数据库账号");
        dataSourceConfig.setPassword("数据库密码");
        // 包配置 文件生成路径
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent("com.yxh.www.template");
        packageConfig.setEntity("domain");
        packageConfig.setMapper("mapper");
        packageConfig.setService("service");
        packageConfig.setServiceImpl("service.impl");
        packageConfig.setController("controller");
        //packageConfig.setXml("mapper.xml");
        // 策略配置
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig.setEntityBuilderModel(true);
        strategyConfig.setEntityLombokModel(true);
        strategyConfig.setEntityTableFieldAnnotationEnable(true);
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setColumnNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setSuperServiceClass("com.yxh.www.common.base.BaseService");
        strategyConfig.setSuperServiceImplClass("com.yxh.www.common.base.BaseServiceImpl");
        strategyConfig.setRestControllerStyle(true);
        // 自定义配置
        InjectionConfig injectionConfig = new InjectionConfig() {
            @Override
            public void initMap() {
            }
        };
        String template = "/templates/mapper.xml.ftl";
        List<FileOutConfig> focList = new ArrayList<>();
        focList.add(new FileOutConfig(template) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return projectPath + "/src/main/resources/mapper/" + tableInfo.getEntityName() + "Mapper.xml";
            }
        });
        injectionConfig.setFileOutConfigList(focList);
        // 设置要创建的表 不设置就是全量生成
        //strategyConfig.setInclude("sm_mail_sender","sm_resource","sm_user");
        // 模板配置
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setXml(null);
        autoGenerator.setGlobalConfig(globalConfig);
        autoGenerator.setDataSource(dataSourceConfig);
        autoGenerator.setPackageInfo(packageConfig);
        autoGenerator.setStrategy(strategyConfig);
        autoGenerator.setCfg(injectionConfig);
        autoGenerator.setTemplate(templateConfig);
        autoGenerator.setTemplateEngine(new FreemarkerTemplateEngine());
        autoGenerator.execute();
    }
}
