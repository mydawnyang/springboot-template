package com.yxh.www.common.base;

import com.baomidou.mybatisplus.extension.service.IService;


/**
 * BaseService操作接口
 * <p>封装统一项目数据库单表操作约束</p>
 * @author yangxiaohui
 */
public interface BaseService<T> extends IService<T> {

}
