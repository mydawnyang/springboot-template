package com.yxh.www.common.mail.client;

/**
 * 自定义多账户邮件发送客户端
 *
 * @author yangxiaohui
 */
public interface MailClient {
    /**
     * 释放所有连接
     */
    void closeAllTransport();

    void sendMessage(String formSenderId, String recipientMail) throws Exception;
}
