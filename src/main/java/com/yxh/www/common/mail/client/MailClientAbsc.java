package com.yxh.www.common.mail.client;

import com.alibaba.fastjson.JSONObject;
import com.yxh.www.common.mail.MailClientConstant;
import com.yxh.www.template.domain.SmMailSender;
import com.yxh.www.template.service.SmMailSenderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.annotation.PreDestroy;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 自定义多账户邮件发送客户端模板类
 *
 * @author yangxiaohui
 */
@Slf4j
public class MailClientAbsc {
    // 发送方SessionMap
    private Map<String, Session> mailSenderSessionMap = new ConcurrentHashMap<>();
    // 发送方邮箱连接
    private Map<String, Transport> mailSenderTransportMap = new ConcurrentHashMap<>();
    // 发送方信息
    private Map<String, SmMailSender> smMailSenderMap = new ConcurrentHashMap<>();

    MailClientAbsc(SmMailSenderService smMailSenderService) {
        buildMailSenders(smMailSenderService);
    }

    private void buildMailSenders(SmMailSenderService mailSenderService) {
        List<SmMailSender> smMailSenders = mailSenderService.list();
        if (null == smMailSenders || smMailSenders.size() <= 0) {
            log.error("无可用Mail发送方账号");
            throw new RuntimeException("无可用Mail发送方账号");
        }
        // 遍历添加所有邮箱发送方，初始化邮件发送器
        for (SmMailSender smMailSender : smMailSenders) {
            Session session = Session.getInstance(new Properties() {{
                // 开启debug模式
                setProperty("mail.debug", "true");
                // 邮箱服务器需要身份验证
                setProperty("mail.smtp.auth", "true");
                // 发送邮件协议名称
                setProperty("mail.transport.protocol", "smtp");
                // 要求 SMTP 连接需要使用 SSL 安全认证 (为了提高安全性, 邮箱支持SSL连接, 也可以自己开启)
                setProperty("mail.smtp.ssl.enable", "true");
                // 设置邮件服务器主机名
                setProperty("mail.host", smMailSender.getMailHost());
                // 设置邮件服务器SSL端口
                setProperty("mail.smtp.port", String.valueOf(smMailSender.getMailPort()));
            }});
            /* 创建邮箱服务器连接 */
            try {
                Transport transport = session.getTransport("smtp");
                // 连接邮件服务器
                transport.connect(smMailSender.getMailHost(), smMailSender.getUserName(), smMailSender.getPassWord());
                mailSenderTransportMap.put(smMailSender.getId(), transport);
            } catch (MessagingException ignored) {
                log.error("发件方初始化连接错误：{}", JSONObject.toJSONString(smMailSender));
                ignored.printStackTrace();
            }
            smMailSenderMap.put(smMailSender.getId(), smMailSender);
            mailSenderSessionMap.put(smMailSender.getId(), session);
        }
    }

    /**
     * 发送简单内容类型邮件
     *
     * @param formSenderId      发送方ID
     * @param title             标题
     * @param content           简单字符串内容
     * @param recipientCcMails  抄送人员地址 Array
     * @param recipientBccMails 密送人员地址 Array
     * @param recipientToMails  接受人员地址 Array
     * @return Message对象
     * @throws MessagingException           MessagingException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    Message buildMessage(String formSenderId, String title, String content, String[] recipientCcMails, String[] recipientBccMails, String... recipientToMails) throws MessagingException, UnsupportedEncodingException {
        Message message = initMessage(formSenderId, title, recipientCcMails, recipientBccMails, recipientToMails);
        if (StringUtils.isNotBlank(content)) {
            message.setContent(content, MailClientConstant.MailContentType.TEXT);
        }
        //保存设置
        message.saveChanges();
        return message;
    }

    /**
     * 发送复杂内容类型邮件
     *
     * @param formSenderId      发送方ID
     * @param title             标题
     * @param content           复杂内容
     * @param recipientCcMails  抄送人员地址 Array
     * @param recipientBccMails 密送人员地址 Array
     * @param recipientToMails  接受人员地址 Array
     * @return Message对象
     * @throws MessagingException           MessagingException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    Message buildMimeMultipartMessage(String formSenderId, String title, MimeMultipart content, String[] recipientCcMails, String[] recipientBccMails, String... recipientToMails) throws MessagingException, UnsupportedEncodingException {
        Message message = initMessage(formSenderId, title, recipientCcMails, recipientBccMails, recipientToMails);
        if (null != content) {
            message.setContent(content);
        }
        //保存设置
        message.saveChanges();
        return message;
    }

    /**
     * 初始化一个没有内容的Message
     *
     * @param formSenderId      发送方ID
     * @param title             标题
     * @param recipientCcMails  抄送人员地址 Array
     * @param recipientBccMails 密送人员地址 Array
     * @param recipientToMails  接受人员地址 Array
     * @return Message对象
     * @throws MessagingException           MessagingException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    Message initMessage(String formSenderId, String title, String[] recipientCcMails, String[] recipientBccMails, String... recipientToMails) throws MessagingException, UnsupportedEncodingException {
        Session session = this.mailSenderSessionMap.get(formSenderId);
        SmMailSender smMailSender = this.smMailSenderMap.get(formSenderId);
        if (null == session) {
            log.error("【{}】未找到有效的会话！", formSenderId);
            throw new RuntimeException("未找到有效的Mail Session会话..");
        }
        Message message = new MimeMessage(session);
        //设置邮件标题
        message.setSubject(title);
        // 设置发送时间
        message.setSentDate(new Date());
        // 设置发件人 将邮件发送给收件人的同时抄送给另一个收件人，收件人可以看到邮件抄送给了谁
        message.setFrom(new InternetAddress(smMailSender.getUserName(), smMailSender.getName()));
        // 设置密件抄送人地址 将邮件发送给收件人的同时将邮件秘密发送给另一个收件人，收件人无法看到邮件密送给了谁
        Address[] recipientBccMailAddress = transitionStrMailToAddresses(recipientBccMails);
        if (null != recipientBccMailAddress) {
            message.setRecipients(Message.RecipientType.BCC, recipientBccMailAddress);
        }
        // 设置抄送人地址
        Address[] recipientCcMailAddress = transitionStrMailToAddresses(recipientCcMails);
        if (null != recipientCcMailAddress) {
            message.setRecipients(Message.RecipientType.CC, recipientCcMailAddress);
        }
        // 设置收件人地址
        Address[] recipientToMailAddress = transitionStrMailToAddresses(recipientToMails);
        if (null != recipientToMailAddress) {
            message.setRecipients(Message.RecipientType.TO, recipientToMailAddress);
        }
        //保存设置
        message.saveChanges();
        return message;
    }

    /**
     * 转换Mails数组到Addresses
     *
     * @param strMails Mails数组
     * @return Addresses
     * @throws AddressException 地址验证异常
     */
    private Address[] transitionStrMailToAddresses(String[] strMails) throws AddressException {
        if (null == strMails || strMails.length <= 0) {
            return null;
        }
        Address[] addresses = new InternetAddress[strMails.length];
        for (int i = 0; i < strMails.length; i++) {
            addresses[i] = new InternetAddress(strMails[i]);
        }
        return addresses;
    }


    /**
     * 获取用户连接器
     *
     * @param formSenderId 发送方id
     * @return Transport
     */
    Transport getFormUserTransport(String formSenderId) {
        Transport transport = this.mailSenderTransportMap.get(formSenderId);
        if (null == transport) {
            log.error("【{}】未找到有效的Transport", formSenderId);
            throw new RuntimeException("【" + formSenderId + "】未找到有效的Transport");
        }
        return transport;
    }

    @PreDestroy
    public void closeAllTransport() {
        try {
            for (Transport transport : this.mailSenderTransportMap.values()) {
                transport.close();
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        log.info("Transport连接释放完毕...");
    }
/*
    public static void main(String[] args) throws Exception{
        //创建邮件
        MimeMessage message = new MimeMessage(Session.getInstance(new Properties()));
        //设置邮件的基本信息
        message.setFrom(new InternetAddress("gacl@sohu.com"));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress("xdp_gacl@sina.cn"));
        message.setSubject("带附件和带图片的的邮件");
        //正文
        MimeBodyPart text = new MimeBodyPart();
        text.setContent("xxx这是女的xxxx<br/><img src='cid:aaa.jpg'>", "text/html;charset=UTF-8");
        //图片
        MimeBodyPart image = new MimeBodyPart();
        image.setDataHandler(new DataHandler(new FileDataSource("src\\3.jpg")));
        image.setContentID("aaa.jpg");
        //附件1
        MimeBodyPart attach = new MimeBodyPart();
        DataHandler dh = new DataHandler(new FileDataSource("src\\4.zip"));
        attach.setDataHandler(dh);
        attach.setFileName(dh.getName());
        //附件2
        MimeBodyPart attach2 = new MimeBodyPart();
        DataHandler dh2 = new DataHandler(new FileDataSource("src\\波子.zip"));
        attach2.setDataHandler(dh2);
        attach2.setFileName(MimeUtility.encodeText(dh2.getName()));
        //描述关系:正文和图片
        MimeMultipart mp1 = new MimeMultipart();
        mp1.addBodyPart(text);
        mp1.addBodyPart(image);
        mp1.setSubType("related");
        //描述关系:正文和附件
        MimeMultipart mp2 = new MimeMultipart();
        mp2.addBodyPart(attach);
        mp2.addBodyPart(attach2);
        //代表正文的bodypart
        MimeBodyPart content = new MimeBodyPart();
        content.setContent(mp1);
        mp2.addBodyPart(content);
        mp2.setSubType("mixed");
        message.setContent(mp2);
        message.saveChanges();
        message.writeTo(new FileOutputStream("E:\\MixedMail.eml"));
        //返回创建好的的邮件
//        return message;
    }
*/
}
