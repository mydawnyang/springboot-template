package com.yxh.www.common.mail.client;

import com.yxh.www.template.service.SmMailSenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.mail.Message;

/**
 * 自定义多账户邮件发送客户端实现
 *
 * @author yangxiaohui
 */
@Slf4j
@Component
public class MailClientImpl extends MailClientAbsc implements MailClient {
    MailClientImpl(SmMailSenderService smMailSenderService) {
        super(smMailSenderService);
    }

    @Override
    public void sendMessage(String formSenderId, String recipientMail) throws Exception {
        String[] strs = new String[2];
        strs[0] = "486466148@qq.com";
        strs[1] = "15836204549@163.com";
        Message message = this.buildMessage(formSenderId, "标题-01", "测试内容测试内容测试内容", null, null, strs);
        this.getFormUserTransport(formSenderId).sendMessage(message, message.getRecipients(Message.RecipientType.TO));
    }
}
