package com.yxh.www.common.oss;

import com.aliyun.oss.ClientBuilderConfiguration;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSBuilder;
import com.aliyun.oss.common.auth.CredentialsProvider;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;

public class AliyunOSSClientBuilder implements OSSBuilder {

    @Override
    public OSS build(String endpoint, String accessKeyId, String secretAccessKey) {
        return new AliyunOSSClient(endpoint, getDefaultCredentialProvider(accessKeyId, secretAccessKey),
                getClientConfiguration());
    }

    @Override
    public OSS build(String endpoint, String accessKeyId, String secretAccessKey, String securityToken) {
        return new AliyunOSSClient(endpoint, getDefaultCredentialProvider(accessKeyId, secretAccessKey, securityToken),
                getClientConfiguration());
    }

    @Override
    public OSS build(String endpoint, String accessKeyId, String secretAccessKey, ClientBuilderConfiguration config) {
        return new AliyunOSSClient(endpoint, getDefaultCredentialProvider(accessKeyId, secretAccessKey),
                getClientConfiguration(config));
    }

    @Override
    public OSS build(String endpoint, String accessKeyId, String secretAccessKey, String securityToken,
                     ClientBuilderConfiguration config) {
        return new AliyunOSSClient(endpoint, getDefaultCredentialProvider(accessKeyId, secretAccessKey, securityToken),
                getClientConfiguration(config));
    }

    @Override
    public OSS build(String endpoint, CredentialsProvider credsProvider) {
        return new AliyunOSSClient(endpoint, credsProvider, getClientConfiguration());
    }

    @Override
    public OSS build(String endpoint, CredentialsProvider credsProvider, ClientBuilderConfiguration config) {
        return new AliyunOSSClient(endpoint, credsProvider, getClientConfiguration(config));
    }

    private static ClientBuilderConfiguration getClientConfiguration() {
        return new ClientBuilderConfiguration();
    }

    private static ClientBuilderConfiguration getClientConfiguration(ClientBuilderConfiguration config) {
        if (config == null) {
            config = new ClientBuilderConfiguration();
        }
        return config;
    }

    private static DefaultCredentialProvider getDefaultCredentialProvider(String accessKeyId, String secretAccessKey) {
        return new DefaultCredentialProvider(accessKeyId, secretAccessKey);
    }

    private static DefaultCredentialProvider getDefaultCredentialProvider(String accessKeyId, String secretAccessKey,
                                                                          String securityToken) {
        return new DefaultCredentialProvider(accessKeyId, secretAccessKey, securityToken);
    }

}
