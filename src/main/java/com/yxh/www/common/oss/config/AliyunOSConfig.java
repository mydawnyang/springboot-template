package com.yxh.www.common.oss.config;

import com.aliyun.oss.ClientBuilderConfiguration;
import com.aliyun.oss.OSS;
import com.yxh.www.common.oss.AliyunOSSClientBuilder;
import com.yxh.www.common.oss.properties.AliyunOSSProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * OSS存储配置
 * @author yangxiaohui
 */
@Slf4j
@Component
public class AliyunOSConfig {
    @Bean(destroyMethod = "shutdown")
    public OSS ossClient(AliyunOSSProperties aliyunOSSProperties) {
        ClientBuilderConfiguration conf = buildOSSConfiguration(aliyunOSSProperties);
        OSS ossClient = new AliyunOSSClientBuilder().build(aliyunOSSProperties.getEndpoint(),aliyunOSSProperties.getAccessKeyId(),aliyunOSSProperties.getAccessKeySecret(),conf);
        log.info("OSSClient初始化完成...");
        log.info("Endpoint:{}",aliyunOSSProperties.getEndpoint());
        log.info("AccessKeyId:{}",aliyunOSSProperties.getAccessKeyId());
        log.info("AccessKeySecret:{}",aliyunOSSProperties.getAccessKeySecret());
        return ossClient;
    }
    /**
     * 初始化OSS存储SDK连接器配置
     * @param properties 获取的配置实体
     * @return OSS存储SDK配置
     */
    private ClientBuilderConfiguration buildOSSConfiguration(AliyunOSSProperties properties) {
        ClientBuilderConfiguration configuration = new ClientBuilderConfiguration();
        configuration.setMaxConnections(properties.getMaxConnections());
        configuration.setSocketTimeout(properties.getSocketTimeout());
        configuration.setConnectionTimeout(properties.getConnectionTimeout());
        configuration.setConnectionRequestTimeout(properties.getConnectionRequestTimeout());
        configuration.setIdleConnectionTime(properties.getIdleConnectionTime());
        configuration.setMaxErrorRetry(properties.getMaxErrorRetry());
        configuration.setSupportCname(properties.getSupportCname());
        configuration.setSLDEnabled(properties.getSLDEnabled());
        configuration.setProtocol(properties.getProtocol());
        configuration.setUserAgent(properties.getUserAgent());
        //configuration.setProxyDomain(properties.getUserAgent());
        //configuration.setProxyWorkstation(properties.getUserAgent());
        if (StringUtils.isNotBlank(properties.getProxyHost())) {
            configuration.setProxyHost(properties.getProxyHost());
        }
        if (StringUtils.isNotBlank(properties.getProxyHost())) {
            configuration.setProxyPort(properties.getProxyPort());
        }
        if (StringUtils.isNotBlank(properties.getProxyUsername())) {
            configuration.setProxyUsername(properties.getProxyUsername());
        }
        if (StringUtils.isNotBlank(properties.getProxyPassword())) {
            configuration.setProxyPassword(properties.getProxyPassword());
        }
        return configuration;
    }
}
