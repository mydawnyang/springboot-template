package com.yxh.www.common.oss.properties;

import com.aliyun.oss.common.comm.Protocol;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "spring.aliyun.oss")
public class AliyunOSSProperties {
    private String endpoint;
    private String accessKeyId;
    private String accessKeySecret;
    //允许打开的最大HTTP连接数。默认为1024
    private int maxConnections=1024;
    // Socket层传输数据的超时时间（单位：毫秒）。默认为60000毫秒
    private int socketTimeout=60000;
    // 建立连接的超时时间（单位：毫秒）。默认为60000毫秒
    private int connectionTimeout=60000;
    // 从连接池中获取连接的超时时间（单位：毫秒）。默认不超时
    private int connectionRequestTimeout=-1;
    // 连接空闲超时时间，超时则关闭连接（单位：毫秒）。默认为60000毫秒
    private long idleConnectionTime=60000L;
    // 请求失败后最大的重试次数。默认5次
    private int maxErrorRetry=5;
    // 是否支持CNAME作为Endpoint，默认支持CNAME
    private boolean supportCname=true;
    // 是否开启二级域名（Second Level Domain）的访问方式，默认不开启
    private boolean SLDEnabled=false;
    // 连接OSS所采用的协议（HTTP/HTTPS），默认为HTTP
    private Protocol protocol=Protocol.HTTP;
    // 用户代理，指HTTP的User-Agent头。默认为”aliyun-sdk-java”
    private String userAgent="aliyun-sdk-java";
    // 代理服务器主机地址
    private String proxyHost;
    // 代理服务器端口
    private int proxyPort;
    // 代理服务器验证的用户名
    private String proxyUsername;
    // 代理服务器验证的密码
    private String proxyPassword;
    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public int getMaxConnections() {
        return maxConnections;
    }

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public int getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getConnectionRequestTimeout() {
        return connectionRequestTimeout;
    }

    public void setConnectionRequestTimeout(int connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
    }

    public long getIdleConnectionTime() {
        return idleConnectionTime;
    }

    public void setIdleConnectionTime(long idleConnectionTime) {
        this.idleConnectionTime = idleConnectionTime;
    }

    public int getMaxErrorRetry() {
        return maxErrorRetry;
    }

    public void setMaxErrorRetry(int maxErrorRetry) {
        this.maxErrorRetry = maxErrorRetry;
    }

    public boolean getSupportCname() {
        return supportCname;
    }

    public void setSupportCname(boolean supportCname) {
        this.supportCname = supportCname;
    }

    public boolean getSLDEnabled() {
        return SLDEnabled;
    }

    public void setSLDEnabled(boolean SLDEnabled) {
        this.SLDEnabled = SLDEnabled;
    }
    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public String getProxyUsername() {
        return proxyUsername;
    }

    public void setProxyUsername(String proxyUsername) {
        this.proxyUsername = proxyUsername;
    }

    public String getProxyPassword() {
        return proxyPassword;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }
}
