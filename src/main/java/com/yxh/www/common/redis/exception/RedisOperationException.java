package com.yxh.www.common.redis.exception;

/**
 * Redis操作异常类
 * @author yangxiaohui
 * @date Create by 2019/12/9 10:00 下午
 */
public class RedisOperationException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    public RedisOperationException(){
        super();
    }
    public RedisOperationException(String msg){
        super(msg);
    }
}
