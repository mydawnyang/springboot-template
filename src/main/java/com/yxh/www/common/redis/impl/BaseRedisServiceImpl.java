package com.yxh.www.common.redis.impl;

import com.yxh.www.common.redis.BaseRedisService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * Redis基础通用操作类
 * @author yangxiaohui
 */
@Slf4j
public class BaseRedisServiceImpl implements BaseRedisService {
    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 设置缓存有效时间 0 永久有效
     * @param key   key
     * @param expire    有效时间 单位-秒
     * @return true:成功 false：失败
     */
    @Override
    public boolean setExpire(String key, long expire){
        try {
            redisTemplate.expire(key, expire, TimeUnit.SECONDS);
        }catch (Exception e){
            log.error("设置缓存失效时间失败 Key：【{}】异常：{}",key,e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 获取缓存剩余有效时间
     * @param key Key
     * @return  -1 代表未找到Key 0 代表永久有效
     */
    @Override
    public long getExpire(String key){
        if (StringUtils.isNotBlank(key)){
            Long value=redisTemplate.getExpire(key);
            if (value!=null){
                return value;
            }else {
                log.warn("Key:【{}】缓存未击中...",key);
            }
        }
        return -1;
    }

    /**
     * 根据Key删除缓存
     * @param key   key
     * @return  true 成功 false 失败
     */
    @Override
    public boolean remove(String key){
        boolean result=false;
        if (StringUtils.isNotBlank(key)){
            try {
                result=redisTemplate.delete(key);
            }catch (Exception e){
                log.error("Key:【{}】缓存删除失败...{}",key,e.getMessage());
                return false;
            }
        }
        return result;
    }
}
