package com.yxh.www.common.redis.impl;

import com.yxh.www.common.redis.RedisMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @author yangxiaohui
 */
@Slf4j
@Component
public class RedisMapServiceImpl extends BaseRedisServiceImpl implements RedisMapService {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Override
    public void put(String key, String hashKey, String value) {
        redisTemplate.opsForHash().put(key,hashKey,value);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T get(String key, String hashKey) {
        Object result=redisTemplate.opsForHash().get(key,hashKey);
        return (T)result;
    }

    @Override
    public void removeByHashKey(String key, String hashKey) {
        redisTemplate.opsForHash().delete(key,hashKey);
    }
}
