package com.yxh.www.common.redis.impl;

import com.alibaba.fastjson.JSONObject;
import com.yxh.www.common.redis.RedisStringService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 简单字符串Redis操作类
 * @author yangxiaohui
 */
@Slf4j
@Component
public class RedisStringServiceImpl extends BaseRedisServiceImpl implements RedisStringService {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Override
    public boolean put(String key, String value){
        try {
            redisTemplate.opsForValue().set(key,value);
        }catch (Exception e){
            log.error("新增缓存失败：Key:{},Value:{},Msg:{}",key, value,e.getMessage());
            return false;
        }
        return true;
    }
    @Override
    public boolean put(String key, String value, long expire ){
        try {
            redisTemplate.opsForValue().set(key,value,expire, TimeUnit.SECONDS);
        }catch (Exception e){
            log.error("新增缓存失败：Key:{},Value:{},Msg:{}",key, value,e.getMessage());
            return false;
        }
        return true;
    }
    @Override
    public String get(String key){
        if (StringUtils.isNotBlank(key)){
            Object value=redisTemplate.opsForValue().get(key);
            if (value!=null){
                return JSONObject.toJSONString(value);
            }else {
                log.error("Key:【{}】缓存未击中...",key);
            }
        }
        return "";
    }
    @Override
    public boolean boundPut(String key,String value){
        try {
            redisTemplate.boundValueOps(key).set(value);
        }catch (Exception e){
            log.error("Bound新增缓存失败：Key:{},Value:{},Msg:{}",key, value,e.getMessage());
            return false;
        }
        return true;
    }
    @Override
    public boolean boundPut(String key,String value,long expire){
        try {
            redisTemplate.boundValueOps(key).set(value,expire,TimeUnit.SECONDS);
        }catch (Exception e){
            log.error("Bound新增缓存失败：Key:{},Value:{},Msg:{}",key, value,e.getMessage());
            return false;
        }
        return true;
    }
    @Override
    public String boundGet(String key){
        if (StringUtils.isNotBlank(key)){
            Object value=redisTemplate.boundValueOps(key).get();
            if (value!=null){
                return JSONObject.toJSONString(value);
            }else {
                log.error("Key:【{}】缓存未击中...",key);
            }
        }
        return "";
    }

}
