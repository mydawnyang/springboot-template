package com.yxh.www.common.util;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * 文件获取SH1值、Md5值
 */
@Slf4j
public class FileDigestUtil {

    public static long infoSh1(InputStream inputStream) throws NoSuchAlgorithmException, IOException {
        byte[] buffer = new byte[1024];
        MessageDigest sh1Digest = MessageDigest.getInstance("SHA-1");
        int len;
        long startTime=new Date().getTime();
        log.info("开始获取SH1：{}", startTime);
        while ((len = inputStream.read(buffer)) != -1) {
            sh1Digest.update(buffer, 0, len);
        }
        BigInteger sh1DigestBigInt = new BigInteger(1, sh1Digest.digest());
        long endTime =new Date().getTime();
        log.info("SH1：{}",sh1DigestBigInt);
        log.info("获取完毕SH1：{}", endTime);
        log.info("耗时：{}", endTime-startTime);
        return 0;
    }

    public static long infoMd5(InputStream inputStream) throws NoSuchAlgorithmException, IOException {
        byte[] buffer = new byte[1024];
        MessageDigest md5Digest = MessageDigest.getInstance("MD5");
        int len;
        long startTime=new Date().getTime();
        log.info("开始获取Md5：{}",startTime );
        while ((len = inputStream.read(buffer)) != -1) {
            md5Digest.update(buffer, 0, len);
        }
        BigInteger md5DigestBigInt = new BigInteger(1, md5Digest.digest());
        long endTime =new Date().getTime();
        log.info("Md5：{}",md5DigestBigInt);
        log.info("获取完毕Md5：{}", endTime);
        log.info("耗时：{}", endTime-startTime);
        return 0;
    }
}