package com.yxh.www.template.config;

import com.yxh.www.template.filter.WebFilterParam;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;

import javax.servlet.MultipartConfigElement;

/**
 * @author yangxiaohui
 * @date Create by 2019/12/26 3:28 下午
 */
@Configuration
public class WebMvcConfig {
    @Bean
    public FilterRegistrationBean WebFilterDemo(){
        //配置过滤器
        FilterRegistrationBean frBean = new FilterRegistrationBean();
        frBean.setFilter(new WebFilterParam());
        frBean.addUrlPatterns("/*");
        frBean.setName("webFilterParam");
        //springBoot会按照order值的大小，从小到大的顺序来依次过滤。
        frBean.setOrder(0);
        return frBean;
    }
    /**
     * 文件上传配置
     */
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //  单个数据大小
        factory.setMaxFileSize(DataSize.ofMegabytes(50));
        /// 总上传数据大小
        factory.setMaxRequestSize(DataSize.ofMegabytes(50));
        return factory.createMultipartConfig();
    }
}
