package com.yxh.www.template.constant;


/**
 * @author yangxiaohui
 * @date Create by 2019/12/26 12:54 上午
 */
public class Constant {
    public static final String SYSTEM_HEADER_TOKEN_NAME="X-Token";
    public static final String SYSTEM_TOKEN_PARAM_USER_ID="requestUserId";
    public static final String SYSTEM_TOKEN_PARAM_USER_NAME="requestUserName";
    public static final String SYSTEM_TOKEN_PARAM_USER_PHONE="requestUserPhone";
    public static final String SYSTEM_TOKEN_PARAM_USER_EMAIL="requestUserEmail";
}
