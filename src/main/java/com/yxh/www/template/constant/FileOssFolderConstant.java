package com.yxh.www.template.constant;

/**
 * 文件夹常量
 * @author yangxiaohui
 * @date Create by 2019/12/26 12:01 上午
 */
public class FileOssFolderConstant {
    public static final String USER_HEADER_PHOTO="userHeaderPhoto/";
}
