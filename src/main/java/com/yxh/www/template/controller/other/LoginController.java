package com.yxh.www.template.controller.other;

import com.yxh.www.common.result.Result;
import com.yxh.www.template.service.SmUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录相关Controller
 * @author yangxiaohui
 */
@Slf4j
@RestController
public class LoginController {
    private SmUserService smUserService;
    public LoginController(SmUserService smUserService){
        this.smUserService=smUserService;
    }
    /**
     * 用户手机号登录
     * @param userPhone 手机号码
     * @param passWord  用户密码
     * @return  响应体
     */
    @PutMapping("/login")
    public Result login(@RequestParam String userPhone, @RequestParam String passWord) {
        return smUserService.login(userPhone,passWord);
    }
}
