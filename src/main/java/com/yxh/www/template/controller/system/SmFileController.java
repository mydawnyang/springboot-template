package com.yxh.www.template.controller.system;

import com.yxh.www.common.result.Result;
import com.yxh.www.template.service.SmFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author yangxiaohui
 * @date Create by 2019/12/26 12:06 上午
 */
@Slf4j
@RestController
@RequestMapping("/smFile")
public class SmFileController {
    private SmFileService smFileService;
    public SmFileController(SmFileService smFileService){
        this.smFileService=smFileService;
    }

    /**
     * 上传用户头像
     * @param userPhoto 用户头像文件
     * @return 响应体
     */
    @PostMapping("/uploadUserPhoto")
    public Result uploadUserPhoto(MultipartFile userPhoto){
        return smFileService.uploadUserPhoto(userPhoto);
    }

    /**
     * 上传项目封面
     * @param projectPhoto  项目封面文件
     * @return 响应体
     */
    @PostMapping("/uploadProjectPhoto")
    public Result uploadProjectPhoto(MultipartFile projectPhoto){
        return smFileService.uploadProjectPhoto(projectPhoto);
    }
}
