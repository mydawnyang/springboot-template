package com.yxh.www.template.controller.system;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yxh.www.common.result.Result;
import com.yxh.www.common.result.ResultBuilder;
import com.yxh.www.template.constant.Constant;
import com.yxh.www.template.service.SmResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 系统资源（菜单）控制器
 *
 * @author yangxiaohui
 */
@Slf4j
@RestController
@RequestMapping("/smResource")
public class SmResourceController {
    private SmResourceService smResourceService;

    SmResourceController(SmResourceService resourceService) {
        this.smResourceService = resourceService;
    }

    /**
     * 增加系统资源（菜单）信息
     *
     * @param resName  资源名称 Title
     * @param resRoute 资源路由
     * @param resLogo  资源Logo
     * @param resId    资源标识
     * @param resType  资源类型0：菜单 1：按钮
     * @param parentId 父级资源ID
     * @param remark   备注
     * @return 响应体
     */
    @PostMapping("/addSmResource")
    public Result addSmResource(@RequestParam String resName,
                                @RequestParam String resRoute,
                                @RequestParam(required = false) String resLogo,
                                @RequestParam String resId,
                                @RequestParam(defaultValue = "0") Integer resType,
                                @RequestParam(required = false) String parentId,
                                @RequestParam(required = false) String remark) {
        return smResourceService.addSmResource(resName, resRoute, resLogo, resId, resType, parentId, remark);
    }

    /**
     * 修改资源信息
     *
     * @param smResourceId 资源ID
     * @param resName      资源名称 Title
     * @param resRoute     资源路由
     * @param resLogo      资源Logo
     * @param resId        资源标识
     * @param resType      资源类型0：菜单 1：按钮
     * @param resStatus    资源状态0：正常；1：失效
     * @param parentId     父级资源ID
     * @param remark       备注
     * @return 响应体
     */
    @PutMapping("/modifySmResource")
    public Result modifySmResource(@RequestParam("id") String smResourceId,
                                   @RequestParam(required = false) String resName,
                                   @RequestParam(required = false) String resRoute,
                                   @RequestParam(required = false) String resLogo,
                                   @RequestParam(required = false) String resId,
                                   @RequestParam(defaultValue = "0") Integer resType,
                                   @RequestParam(defaultValue = "0") Integer resStatus,
                                   @RequestParam(required = false) String parentId,
                                   @RequestParam(required = false) String remark) {
        return smResourceService.modifySmResource(smResourceId, resName, resRoute, resLogo, resId, resType, resStatus, parentId, remark);
    }

    /**
     * 删除系统资源
     *
     * @param smResourceId 资源ID
     * @return 响应体
     */
    @DeleteMapping("/removeSmResource")
    public Result removeSmResource(@RequestParam String smResourceId) {
        return smResourceService.removeSmResource(smResourceId);
    }

    /**
     * 删除多个系统资源
     * @param smResourceIds 系统资源ID集合
     * @return  响应体
     */
    @DeleteMapping("/removeSmResources")
    public Result removeSmResources(@RequestParam String[] smResourceIds) {
        return smResourceService.removeSmResources(smResourceIds);
    }

    /**
     * 分页查询资源
     *
     * @param pageNum  页码
     * @param pageSize 页面大小
     * @return 响应体
     */
    @GetMapping("/listPageSmResource")
    public Result listPageSmResource(@RequestParam(defaultValue = "1") Long pageNum,
                                     @RequestParam(defaultValue = "10") Long pageSize) {
        return ResultBuilder.success(smResourceService.page(new Page<>(pageNum, pageSize)));
    }

    /**
     * 获取所有资源 树形结果返回
     * @return 响应体
     */
    @GetMapping("/treeAllSmResource")
    public Result treeAllSmResource(){
        return smResourceService.treeAllSmResource();
    }

    /**
     * 获取用户资源 树形结果返回
     * @param smUserId  用户ID 从Token取，不需要传入
     * @return 响应体
     */
    @GetMapping("/treeSmResourceByUserId")
    public Result treeSmResourceByUserId(@RequestParam(name = Constant.SYSTEM_TOKEN_PARAM_USER_ID) String smUserId){
        return smResourceService.treeSmResourceByUserId(smUserId);
    }

    /**
     * 根据角色ID获取角色的所有资源 树形结果返回
     * @param smRoleId 角色ID
     * @return 响应体
     */
    @GetMapping("/treeSmResourceByRoleId")
    public Result treeSmResourceByRoleId(@RequestParam(required = false) String smRoleId){
        return smResourceService.treeSmResourceByRoleId(smRoleId);
    }
    /**
     * 获取角色拥有的资源ID集合
     * @param smRoleId 角色ID
     * @return  响应体
     */
    @GetMapping("/listSmResourceId")
    public Result listSmResourceId(@RequestParam(required = false) String smRoleId){
        return smResourceService.listSmResourceId(smRoleId);
    }

    /**
     * 获取角色变配资源 已拥有的资源和未分配的资源树结构列表
     * @param smRoleId 角色ID
     * @return  响应体
     */
    @GetMapping("/treeSmRoleSmResourceRelation")
    public Result treeSmRoleSmResourceRelation(@RequestParam(required = false) String smRoleId){
        return smResourceService.treeSmRoleSmResourceRelation(smRoleId);
    }
}
