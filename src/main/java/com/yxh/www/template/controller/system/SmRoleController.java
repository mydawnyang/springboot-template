package com.yxh.www.template.controller.system;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yxh.www.common.result.Result;
import com.yxh.www.common.result.ResultBuilder;
import com.yxh.www.template.constant.Constant;
import com.yxh.www.template.service.SmRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author yangxiaohui
 * @date Create by 2019/12/15 11:15 下午
 */
@Slf4j
@RestController
@RequestMapping("/smRole")
public class SmRoleController {
    private SmRoleService smRoleService;

    SmRoleController(SmRoleService smRoleService) {
        this.smRoleService = smRoleService;
    }

    /**
     * 新增系统角色
     *
     * @param roleName 角色名称
     * @param remark   角色备注
     * @return 响应体
     */
    @PostMapping("/addSmRole")
    public Result addSmRole(@RequestParam String roleName, @RequestParam(required = false) String remark, @RequestParam(name = Constant.SYSTEM_TOKEN_PARAM_USER_ID) String requestUserId) {
        return smRoleService.addSmRole(roleName, remark, requestUserId);
    }

    /**
     * 修改系统角色
     *
     * @param id       角色ID
     * @param roleName 角色名称
     * @param remark   角色备注
     * @return 响应体
     */
    @PutMapping("/modifySmRole")
    public Result modifySmRole(@RequestParam String id,
                               @RequestParam(required = false) String roleName,
                               @RequestParam(required = false) String remark) {
        return smRoleService.modifySmRole(id, roleName, remark);
    }

    /**
     * 删除角色
     *
     * @param smRoleId 角色ID
     * @return 响应体
     */
    @DeleteMapping("/removeSmRole")
    public Result removeSmRole(@RequestParam String smRoleId) {
        return smRoleService.removeSmRole(smRoleId);
    }

    /**
     * 删除多个角色
     *
     * @param smRoleIds 角色ID集合
     * @return 响应体
     */
    @DeleteMapping("/removeSmRoles")
    public Result removeSmRoles(@RequestParam String[] smRoleIds) {
        return smRoleService.removeSmRoles(smRoleIds);
    }


    /**
     * 分页查询角色
     *
     * @param pageNum  页码
     * @param pageSize 页面大小
     * @return 响应体
     */
    @GetMapping("/listPageSmRole")
    public Result listPageSmRole(@RequestParam(defaultValue = "1") Long pageNum,
                                 @RequestParam(defaultValue = "10") Long pageSize) {
        return ResultBuilder.success(smRoleService.page(new Page<>(pageNum, pageSize)));
    }

    /**
     * 获取所有角色列表
     * @return  响应体
     */
    @GetMapping("/listAllSmRole")
    public Result listAllSmRole(){
        return smRoleService.listAllSmRoles();
    }
}
