package com.yxh.www.template.controller.system;

import com.yxh.www.common.result.Result;
import com.yxh.www.template.constant.Constant;
import com.yxh.www.template.service.SmRoleResourceRelationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 系统角色资源关系控制器
 *
 * @author yangxiaohui
 */
@Slf4j
@RestController
@RequestMapping("/smRoleResourceRelation")
public class SmRoleResourceRelationController {
    private SmRoleResourceRelationService smRoleResourceRelationService;

    public SmRoleResourceRelationController(SmRoleResourceRelationService smRoleResourceRelationService) {
        this.smRoleResourceRelationService = smRoleResourceRelationService;
    }

    /**
     * 新增角色关联资源关系
     *
     * @param roleId     角色ID
     * @param resourceId 资源ID
     * @return 响应体
     */
    @PostMapping("/addSmRoleResourceRelation")
    public Result addSmRoleResourceRelation(@RequestParam String roleId, @RequestParam String resourceId) {
        return smRoleResourceRelationService.addSmRoleResourceRelation(roleId, resourceId);
    }

    /**
     * 增加角色资源关系
     * @param roleId    角色ID
     * @param resourceIds   资源ID集合
     * @param requestUserId 请求用户
     * @return  响应体
     */
    @PostMapping("/addSmRoleResourcesRelation")
    public Result addSmRoleResourcesRelation(@RequestParam String roleId,
                                             @RequestParam String[] resourceIds,
                                             @RequestParam(name=Constant.SYSTEM_TOKEN_PARAM_USER_ID)String requestUserId) {
        return smRoleResourceRelationService.addSmRoleResourcesRelation(roleId, resourceIds,requestUserId);
    }

    /**
     * 修改角色资源关系
     * @param roleId    角色ID
     * @param resourceIds   资源ID
     * @param requestUserId 请求用户
     * @return
     */
    @PutMapping("/modifySmRoleResourcesRelation")
    public Result modifySmRoleResourcesRelation(@RequestParam String roleId,
                                                @RequestParam(required = false) String[] resourceIds,
                                                @RequestParam(name=Constant.SYSTEM_TOKEN_PARAM_USER_ID)String requestUserId) {
        return smRoleResourceRelationService.modifySmRoleResourcesRelation(roleId, resourceIds,requestUserId);
    }

    /**
     * 删除角色资源关系
     *
     * @param smRoleResourceRelationId 角色资源关系ID
     * @return 响应体
     */
    @DeleteMapping("/removeSmRoleResourceRelation")
    public Result removeSmRoleResourceRelation(@RequestParam String smRoleResourceRelationId) {
        return smRoleResourceRelationService.removeSmRoleResourceRelation(smRoleResourceRelationId);
    }
    /**
     * 删除多个角色资源关系
     *
     * @param roleId 角色ID
     * @param resourceIds 资源ID集合
     * @return 响应体
     */
    @DeleteMapping("/removeSmRoleResourcesRelation")
    public Result removeSmRoleResourcesRelation(@RequestParam String roleId,
                                                @RequestParam String[] resourceIds) {
        return smRoleResourceRelationService.removeSmRoleResourcesRelation(roleId,resourceIds);
    }




}
