package com.yxh.www.template.controller.system;

import com.yxh.www.common.result.Result;
import com.yxh.www.template.service.SmUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 系统用户管理相关API
 * @author yangxiaohui
 * @date Create by 2019/12/15 7:38 下午
 */
@Slf4j
@RestController
@RequestMapping("/smUser")
public class SmUserController {
    private SmUserService smUserService;
    SmUserController(SmUserService smUserService){this.smUserService=smUserService;}

    /**
     * 增加系统用户
     * @param userPhoto 用户头像
     * @param userName  用户名称
     * @param userPhone 用户手机号
     * @param loginPass 登录密码 默认 123456789
     * @param userEmail 用户邮箱
     * @return  响应体
     */
    @PostMapping("/addSmUser")
    public Result addSmUser(@RequestParam(required = false)String userPhoto,
                            @RequestParam String userName,
                            @RequestParam String userPhone,
                            @RequestParam(defaultValue = "123456789")String loginPass,
                            @RequestParam(required = false)String userEmail){
        return smUserService.addSmUser(userPhoto, userName, userPhone, loginPass, userEmail);
    }

    /**
     * 修改系统用户
     * @param smUserId 用户ID
     * @param userPhoto 用户头像
     * @param userName  用户名称
     * @param userPhone 用户手机号
     * @param loginPass 登录密码
     * @param userEmail 用户邮箱
     * @return 响应体
     */
    @PutMapping("/modifySmUser")
    public Result modifySmUser(@RequestParam("id") String smUserId,
                               @RequestParam(required = false)String userPhoto,
                               @RequestParam(required = false) String userName,
                               @RequestParam (required = false)String userPhone,
                               @RequestParam(required = false)String loginPass,
                               @RequestParam(required = false)String userEmail,
                               @RequestParam(required = false)Integer userStatus){
        return smUserService.modifySmUser(smUserId, userPhoto, userName, userPhone, loginPass, userEmail,userStatus);
    }

    /**
     * 分页查询系统用户列表
     * @param userName  用户名称
     * @param userPhone 用户手机号
     * @param userEmail 用户邮箱
     * @param pageNum   页码
     * @param pageSize  页面数据长度
     * @return  系统用户列表
     */
    @GetMapping("/listPageSmUser")
    public Result listPageSmUser(@RequestParam(defaultValue = "1")Long pageNum,
                                 @RequestParam(defaultValue = "10")Long pageSize,
                                 @RequestParam(defaultValue = "")String userName,
                                 @RequestParam(defaultValue = "")String userPhone,
                                 @RequestParam(defaultValue = "")String userEmail){
        return smUserService.listPageSmUser(userName, userPhone, userEmail, pageNum, pageSize);
    }

    /**
     * 删除用户
     * @param id 用户ID
     * @return 响应体
     */
    @DeleteMapping("/removeSmUser")
    public Result removeSmUser(@RequestParam String id){
        return smUserService.removeSmUsers(id);
    }

    /**
     * 删除多个用户
     * @param ids 用户id集合
     * @return 响应体
     */
    @DeleteMapping("/removeSmUsers")
    public Result removeSmUsers(@RequestParam String[] ids){
        return smUserService.removeSmUsers(ids);
    }
}
