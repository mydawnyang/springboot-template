package com.yxh.www.template.controller.system;

import com.yxh.www.common.result.Result;
import com.yxh.www.template.service.SmUserRoleRelationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author yangxiaohui
 * @date Create by 2019/12/16 2:10 下午
 */
@Slf4j
@RestController
@RequestMapping("/smUserRoleRelation")
public class SmUserRoleRelationController {
    private SmUserRoleRelationService smUserRoleRelationService;
    public SmUserRoleRelationController(SmUserRoleRelationService smUserRoleRelationService) {
        this.smUserRoleRelationService = smUserRoleRelationService;
    }

    /**
     * 新增用户角色关系信息
     *
     * @param userId 用户id
     * @param roleId 角色id
     * @return 响应体
     */
    @PostMapping("/addSmUserRoleRelation")
    public Result addSmUserRoleRelation(@RequestParam String userId, @RequestParam String roleId) {
        return smUserRoleRelationService.addSmUserRoleRelation(userId, roleId);
    }

    /**
     * 删除用户角色关系
     *
     * @param smUserRoleRelationId 用户角色关系ID
     * @return 响应体
     */
    @DeleteMapping("/removeSmUserRoleRelation")
    public Result removeSmUserRoleRelation(@RequestParam String smUserRoleRelationId) {
        return smUserRoleRelationService.removeSmUserRoleRelation(smUserRoleRelationId);
    }

    /**
     * 更新用户角色列表
     * @param smUserId  用户ID
     * @param roleIds   角色列表
     * @param requestUserId 请求用户ID
     * @return  响应体
     */
    @PutMapping("/modifySmUserRoleRelation")
    public Result modifySmUserRoleRelation(@RequestParam String smUserId, @RequestParam(required = false) String[] roleIds, @RequestParam String requestUserId){
        return smUserRoleRelationService.modifySmUserRoleRelation(smUserId, roleIds, requestUserId);
    }
}
