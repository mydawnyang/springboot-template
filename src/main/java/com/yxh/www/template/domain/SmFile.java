package com.yxh.www.template.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.yxh.www.common.util.UUIDUtil;
import com.yxh.www.template.enums.FileOssFolderEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

/**
 * <p>
 * 系统文件表
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SmFile extends Model<SmFile> {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 原文件名
     */
    @TableField("old_file_name")
    private String oldFileName;

    /**
     * 文件名
     */
    @TableField("file_name")
    private String fileName;

    /**
     * HASH唯一标识
     */
    @TableField("file_hash")
    private String fileHash;

    /**
     * 文件格式 （后缀）
     */
    @TableField("file_format")
    private String fileFormat;

    /**
     * 文件大小 （字节单位）
     */
    @TableField("file_size")
    private Long fileSize;

    /**
     * 所属文件夹
     */
    @TableField("folder_id")
    private String folderId;

    /**
     * 逻辑路径
     */
    @TableField("logical_path")
    private String logicalPath;

    /**
     * 物理路径
     */
    @TableField("physics_path")
    private String physicsPath;


    public SmFile(){}
    public SmFile(FileOssFolderEnum folderEnum, MultipartFile webFile){
        // 原文件名
        String fileOldName=webFile.getOriginalFilename();
        this.oldFileName=fileOldName;
        // 新文件名
        this.fileName= UUIDUtil.buildUUID_32Upper()+fileOldName.substring(fileOldName.lastIndexOf("."));
        // 存储路径
        this.logicalPath=folderEnum.getFolderPath()+"/"+this.fileName;
        this.physicsPath=folderEnum.getFolderPath()+"/"+this.fileName;
        // 文件大小
        this.fileSize=webFile.getSize();
        // 文件格式
        this.fileFormat=fileOldName.substring(fileOldName.lastIndexOf(".")+1);
        this.folderId=folderEnum.getId();
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
