package com.yxh.www.template.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统文件夹表
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SmFolder extends Model<SmFolder> {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 文件夹名称
     */
    @TableField("folder_name")
    private String folderName;

    /**
     * 父级ID
     */
    @TableField("parent_id")
    private String parentId;

    /**
     * 逻辑路径
     */
    @TableField("folder_path")
    private String folderPath;

    /**
     * 创建人员
     */
    @TableField("create_user")
    private String createUser;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 文件夹类型 （0：系统文件夹；1：用户文件夹）
     */
    @TableField("folder_type")
    private Integer folderType;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
