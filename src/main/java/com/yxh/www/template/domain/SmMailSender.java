package com.yxh.www.template.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 邮件发送方表
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-12-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SmMailSender extends Model<SmMailSender> {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 邮件服务方地址
     */
    @TableField("mail_host")
    private String mailHost;

    /**
     * 邮件发送端口
     */
    @TableField("mail_port")
    private String mailPort;

    /**
     * 发送方昵称
     */
    @TableField("mail_form")
    private String mailForm;

    /**
     * 邮件发送方用户名-邮箱地址
     */
    @TableField("user_name")
    private String userName;

    /**
     * 第三方授权码或者密码
     */
    @TableField("pass_word")
    private String passWord;

    /**
     * 默认邮件编码
     */
    @TableField("default_encoding")
    private String defaultEncoding;

    @TableField("name")
    private String name;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
