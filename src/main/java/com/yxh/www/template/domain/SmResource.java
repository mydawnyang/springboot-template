package com.yxh.www.template.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 系统资源表
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SmResource extends Model<SmResource> {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 资源名称
     */
    @TableField("res_name")
    private String resName;

    /**
     * 资源路由
     */
    @TableField("res_route")
    private String resRoute;

    /**
     * 资源LOGO
     */
    @TableField("res_logo")
    private String resLogo;

    /**
     * 资源标识
     */
    @TableField("res_id")
    private String resId;

    /**
     * 资源类型 （0：菜单；1：按钮）
     */
    @TableField("res_type")
    private Integer resType;

    /**
     * 资源状态 （0：正常；1：失效）
     */
    @TableField("res_status")
    private Integer resStatus;

    /**
     * 资源备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 父级ID
     */
    @TableField("parent_id")
    private String parentId;

    public SmResource() {
    }

    public SmResource(String resName, String resRoute, String resLogo, String resId, Integer resType, String parentId, String remark) {
        this.resName = resName;
        this.resRoute = resRoute;
        this.resLogo = resLogo;
        this.resId = resId;
        this.resType = resType;
        this.parentId = parentId;
        this.remark = remark;
    }

    public SmResource(String smResourceId, String resName, String resRoute, String resLogo, String resId, Integer resType, Integer resStatus, String parentId, String remark) {
        this.id = smResourceId;
        this.resName = resName;
        this.resRoute = resRoute;
        this.resLogo = resLogo;
        this.resId = resId;
        this.resType = resType;
        this.resStatus = resStatus;
        this.parentId = parentId;
        this.remark = remark;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
