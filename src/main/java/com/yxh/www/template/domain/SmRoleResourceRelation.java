package com.yxh.www.template.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统角色资源关系表
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SmRoleResourceRelation extends Model<SmRoleResourceRelation> {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 角色ID
     */
    @TableField("role_id")
    private String roleId;

    /**
     * 资源ID
     */
    @TableField("resource_id")
    private String resourceId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 创建人员
     */
    @TableField("create_user")
    private String createUser;


    public SmRoleResourceRelation(){}
    public SmRoleResourceRelation(String roleId, String resourceId){
        this.resourceId=resourceId;
        this.roleId=roleId;
    }
    public SmRoleResourceRelation(String roleId, String resourceId,String createUser,LocalDateTime createTime){
        this.resourceId=resourceId;
        this.roleId=roleId;
        this.createTime=createTime;
        this.createUser=createUser;
    }
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
