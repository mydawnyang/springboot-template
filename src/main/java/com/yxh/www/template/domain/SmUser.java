package com.yxh.www.template.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SmUser extends Model<SmUser> {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 用户头像
     */
    @TableField("user_photo")
    private String userPhoto;

    /**
     * 用户名称
     */
    @TableField("user_name")
    private String userName;

    /**
     * 手机号码
     */
    @TableField("user_phone")
    private String userPhone;

    /**
     * 登录密码
     */
    @TableField("login_pass")
    private String loginPass;

    /**
     * 电子邮箱
     */
    @TableField("user_email")
    private String userEmail;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 最后登陆时间
     */
    @TableField("last_login_time")
    private LocalDateTime lastLoginTime;

    /**
     * 用户状态 （0：正常；1：失效）
     */
    @TableField("user_status")
    private Integer userStatus;

    public SmUser(){};
    public SmUser(String id,String userPhoto, String userName, String userPhone, String loginPass, String userEmail){
        this.id=id;
        this.userPhoto=userPhoto;
        this.userName=userName;
        this.userPhone=userPhone;
        this.loginPass=loginPass;
        this.userEmail=userEmail;
    }
    public SmUser(String id,String userPhoto, String userName, String userPhone, String loginPass, String userEmail,Integer userStatus){
        this.id=id;
        this.userPhoto=userPhoto;
        this.userName=userName;
        this.userPhone=userPhone;
        this.loginPass=loginPass;
        this.userEmail=userEmail;
        this.userStatus=userStatus;
    }
    public SmUser(String userPhoto, String userName, String userPhone, String loginPass, String userEmail){
        this.userPhoto=userPhoto;
        this.userName=userName;
        this.userPhone=userPhone;
        this.loginPass=loginPass;
        this.userEmail=userEmail;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
