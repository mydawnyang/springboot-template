package com.yxh.www.template.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统用户角色关系表
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SmUserRoleRelation extends Model<SmUserRoleRelation> {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private String userId;

    /**
     * 角色ID
     */
    @TableField("role_id")
    private String roleId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 创建人员
     */
    @TableField("create_user")
    private String createUser;


    public SmUserRoleRelation(){}
    public SmUserRoleRelation(String userId, String roleId){
        this.userId=userId;
        this.roleId=roleId;
    }

    public SmUserRoleRelation(String userId,String roleId,String createUser,LocalDateTime createTime){
        this.userId=userId;
        this.roleId=roleId;
        this.createUser=createUser;
        this.createTime=createTime;
    }
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
