package com.yxh.www.template.dto;

import com.yxh.www.template.domain.SmResource;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangxiaohui
 * @date Create by 2020/1/7 12:33 下午
 */
@Data
public class SmResourceDTO extends SmResource implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<SmResourceDTO> children;
    public SmResourceDTO(){}
    public SmResourceDTO(SmResource smResource){
        this.setId(smResource.getId());
        this.setResName(smResource.getResName());
        this.setResRoute(smResource.getResRoute());
        this.setResLogo(smResource.getResLogo());
        this.setResId(smResource.getResId());
        this.setResStatus(smResource.getResStatus());
        this.setResType(smResource.getResType());
        this.setRemark(smResource.getRemark());
    }
}
