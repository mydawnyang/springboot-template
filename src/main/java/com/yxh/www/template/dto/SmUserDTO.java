package com.yxh.www.template.dto;

import com.yxh.www.template.domain.SmUser;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yangxiaohui
 * @date Create by 2020/1/6 10:40 上午
 */
@Data
public class SmUserDTO extends SmUser implements Serializable {
    private static final long serialVersionUID = 1L;
    private String[] roleIds;
}
