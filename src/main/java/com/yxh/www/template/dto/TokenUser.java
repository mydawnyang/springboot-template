package com.yxh.www.template.dto;

import com.yxh.www.template.domain.SmUser;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yangxiaohui
 * @date Create by 2019/12/16 2:44 下午
 */
@Data
public class TokenUser extends SmUser implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<VueMenu> resources;
    private String token;
    private String md5Key;
    public TokenUser(SmUser smUser){
        this.setId(smUser.getId());
        this.setUserPhoto(smUser.getUserPhoto());
        this.setUserPhone(smUser.getUserPhone());
        this.setUserName(smUser.getUserName());
        this.setUserEmail(smUser.getUserEmail());
        this.setLoginPass(smUser.getLoginPass());
        this.setCreateTime(smUser.getCreateTime());
        this.setLastLoginTime(smUser.getLastLoginTime());
        this.setUserStatus(smUser.getUserStatus());
    }
    public TokenUser(SmUser smUser, List<VueMenu> resources){
        this.setId(smUser.getId());
        this.setUserPhoto(smUser.getUserPhoto());
        this.setUserPhone(smUser.getUserPhone());
        this.setUserName(smUser.getUserName());
        this.setUserEmail(smUser.getUserEmail());
        this.setLoginPass(smUser.getLoginPass());
        this.setCreateTime(smUser.getCreateTime());
        this.setLastLoginTime(smUser.getLastLoginTime());
        this.setUserStatus(smUser.getUserStatus());
        this.resources=resources;
    }

    @Override
    public String getId() {
        return super.getId();
    }

    @Override
    public String getUserPhoto() {
        return super.getUserPhoto();
    }

    @Override
    public String getUserName() {
        return super.getUserName();
    }

    @Override
    public String getUserPhone() {
        return super.getUserPhone();
    }

    @Override
    public String getLoginPass() {
        return super.getLoginPass();
    }

    @Override
    public String getUserEmail() {
        return super.getUserEmail();
    }

    @Override
    public LocalDateTime getCreateTime() {
        return super.getCreateTime();
    }

    @Override
    public LocalDateTime getLastLoginTime() {
        return super.getLastLoginTime();
    }

    @Override
    public Integer getUserStatus() {
        return super.getUserStatus();
    }

    @Override
    public SmUser setId(String id) {
        return super.setId(id);
    }

    @Override
    public SmUser setUserPhoto(String userPhoto) {
        return super.setUserPhoto(userPhoto);
    }

    @Override
    public SmUser setUserName(String userName) {
        return super.setUserName(userName);
    }

    @Override
    public SmUser setUserPhone(String userPhone) {
        return super.setUserPhone(userPhone);
    }

    @Override
    public SmUser setLoginPass(String loginPass) {
        return super.setLoginPass(loginPass);
    }

    @Override
    public SmUser setUserEmail(String userEmail) {
        return super.setUserEmail(userEmail);
    }

    @Override
    public SmUser setCreateTime(LocalDateTime createTime) {
        return super.setCreateTime(createTime);
    }

    @Override
    public SmUser setLastLoginTime(LocalDateTime lastLoginTime) {
        return super.setLastLoginTime(lastLoginTime);
    }

    @Override
    public SmUser setUserStatus(Integer userStatus) {
        return super.setUserStatus(userStatus);
    }

    public TokenUser(String id, String userPhoto, String userName, String userPhone, String loginPass, String userEmail) {
        super(id, userPhoto, userName, userPhone, loginPass, userEmail);
    }

    public TokenUser(String userPhoto, String userName, String userPhone, String loginPass, String userEmail) {
        super(userPhoto, userName, userPhone, loginPass, userEmail);
    }

    @Override
    protected Serializable pkVal() {
        return super.pkVal();
    }
}
