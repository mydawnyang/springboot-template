package com.yxh.www.template.dto;

import com.yxh.www.template.domain.SmResource;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangxiaohui
 * @date Create by 2019/12/16 3:38 下午
 */
@Data
public class VueMenu implements Serializable {
    private static final long serialVersionUID = 1L;
    private String title;
    private String path;
    private String icon;
    private String id;
    private String pid;
    private List<VueMenu> children;

    public VueMenu(){}

    public VueMenu(SmResource smResource){
        this.title=smResource.getResName();
        this.path="/"+smResource.getResRoute();
        this.icon=smResource.getResLogo();
        this.id=smResource.getId();
        this.pid=smResource.getParentId();
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = "/"+path;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public List<VueMenu> getChildren() {
        return children;
    }

    public void setChildren(List<VueMenu> children) {
        this.children = children;
    }
}
