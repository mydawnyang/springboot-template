package com.yxh.www.template.enums;

/**
 * @author yangxiaohui
 * @date Create by 2020/1/9 11:58 上午
 */
public enum FileOssFolderEnum {
    // 用户头像文件夹
    USER_PHOTO_FOLDER("908EB498B0DE493591779A029E1CD83A","/user/photo"),
    // 项目封面文件夹
    PROJECT_PHOTO_FOLDER("B1906B4BF7784064AF445520E66561CF","/project/photo");
    private String id;
    private String folderPath;
    FileOssFolderEnum(String id,String folderPath){
        this.folderPath=folderPath;
        this.id=id;
    }
    public String getId() {
        return id;
    }
    public String getFolderPath() {
        return folderPath;
    }
}
