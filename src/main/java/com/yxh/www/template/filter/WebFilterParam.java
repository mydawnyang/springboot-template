package com.yxh.www.template.filter;

import com.yxh.www.template.constant.Constant;
import com.yxh.www.template.dto.TokenUser;
import com.yxh.www.template.util.JwtTokenUtil;
import com.yxh.www.template.util.ParameterServletRequestWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yangxiaohui
 * @date Create by 2019/12/26 3:27 下午
 */
public class WebFilterParam extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //修改请求参数
        Map<String,String[]> params=new HashMap<>(request.getParameterMap());
        String requestTokenStr = request.getHeader(Constant.SYSTEM_HEADER_TOKEN_NAME);
        if (StringUtils.isNotBlank(requestTokenStr)) {
            try {
                TokenUser requestUser = JwtTokenUtil.parseJWTToUser(requestTokenStr);
                params.put(Constant.SYSTEM_TOKEN_PARAM_USER_ID, new String[]{requestUser.getId()});
                params.put(Constant.SYSTEM_TOKEN_PARAM_USER_NAME, new String[]{requestUser.getUserName()});
                params.put(Constant.SYSTEM_TOKEN_PARAM_USER_PHONE, new String[]{requestUser.getUserPhone()});
                params.put(Constant.SYSTEM_TOKEN_PARAM_USER_EMAIL, new String[]{requestUser.getUserEmail()});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ParameterServletRequestWrapper req = new ParameterServletRequestWrapper(request,params);
        //调用对应的controller
        super.doFilter(req,response,filterChain);
        // 修改响应信息
    }
}
