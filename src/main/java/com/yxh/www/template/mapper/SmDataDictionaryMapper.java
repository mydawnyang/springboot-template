package com.yxh.www.template.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxh.www.template.domain.SmDataDictionary;

/**
 * <p>
 * 系统数据字典表 Mapper 接口
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmDataDictionaryMapper extends BaseMapper<SmDataDictionary> {

}
