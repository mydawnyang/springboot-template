package com.yxh.www.template.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxh.www.template.domain.SmMailSender;

/**
 * <p>
 * 邮件发送方表 Mapper 接口
 * </p>
 * @author Yangxiaohui
 * @since 2019-12-11
 */
public interface SmMailSenderMapper extends BaseMapper<SmMailSender> {

}
