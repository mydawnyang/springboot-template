package com.yxh.www.template.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxh.www.template.domain.SmOrganization;

/**
 * <p>
 * 系统组织表 Mapper 接口
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmOrganizationMapper extends BaseMapper<SmOrganization> {

}
