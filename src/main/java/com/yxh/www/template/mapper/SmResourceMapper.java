package com.yxh.www.template.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxh.www.template.domain.SmResource;
import com.yxh.www.template.dto.SmResourceDTO;
import lombok.NonNull;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统资源表 Mapper 接口
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmResourceMapper extends BaseMapper<SmResource> {
    /**
     * 查询用户角色下的有效资源信息
     * @param userId    用户ID
     * @return  用户资源列表
     */
    List<SmResource> listResourceByUserId(@NonNull @Param("userId") String userId);

    /**
     * 查询角色下的有效资源信息
     * @param roleId    角色ID
     * @return  角色资源列表
     */
    List<SmResource> listResourceByRoleId(@NonNull @Param("roleId") String roleId);

    /**
     * 根据资源ID过滤资源
     * @param smResourceIds 资源ID集合
     * @return 资源列表
     */
    List<SmResource> listResourceByNotInSmResourceIds(@Param("smResourceIds") List<String> smResourceIds);

    /**
     * 获取子级资源列表
     * @param parentId  父ID
     * @return  SmResourceDTO列表
     */
    List<SmResourceDTO> listSmResourceChildren(@Param("parentId") String parentId);
}
