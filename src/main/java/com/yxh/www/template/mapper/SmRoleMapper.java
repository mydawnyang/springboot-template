package com.yxh.www.template.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxh.www.template.domain.SmRole;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmRoleMapper extends BaseMapper<SmRole> {

}
