package com.yxh.www.template.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxh.www.template.domain.SmUserLabelRelation;

/**
 * <p>
 * 用户标签关系表 Mapper 接口
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmUserLabelRelationMapper extends BaseMapper<SmUserLabelRelation> {

}
