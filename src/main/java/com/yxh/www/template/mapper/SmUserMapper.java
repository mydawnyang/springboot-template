package com.yxh.www.template.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yxh.www.template.domain.SmUser;
import com.yxh.www.template.dto.SmUserDTO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmUserMapper extends BaseMapper<SmUser> {
   /**
    * 检查密码是否正确
    * @param userPhone  用户手机号
    * @param loginPass  用户密码
    * @return  用户条数
    */
   int checkUserPass(@Param("userPhone") String userPhone, @Param("loginPass") String loginPass);

   /**
    * 根据条件分页查询用户
    * @param page   分页条件
    * @param userName   用户名
    * @param userPhone  用户手机号
    * @param userEmail  用户邮箱
    * @return  {@see SmUserDTO}
    */
   IPage<SmUserDTO> listPageSmUser(Page<SmUserDTO> page, @Param("userName") String userName, @Param("userPhone") String userPhone, @Param("userEmail") String userEmail);
}
