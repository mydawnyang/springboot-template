package com.yxh.www.template.service;

import com.yxh.www.common.base.BaseService;
import com.yxh.www.template.domain.SmDataDictionary;

/**
 * <p>
 * 系统数据字典表 服务类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmDataDictionaryService extends BaseService<SmDataDictionary> {

}
