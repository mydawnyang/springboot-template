package com.yxh.www.template.service;

import com.yxh.www.common.base.BaseService;
import com.yxh.www.common.result.Result;
import com.yxh.www.template.domain.SmFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 系统文件表 服务类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmFileService extends BaseService<SmFile> {
    /**
     * 上传用户头像
     * @param userPhoto 用户头像文件
     * @return 响应体
     */
    Result uploadUserPhoto(MultipartFile userPhoto);

    /**
     * 上传项目封面
     * @param projectPhoto  项目封面文件
     * @return 响应体
     */
    Result uploadProjectPhoto(MultipartFile projectPhoto);
}
