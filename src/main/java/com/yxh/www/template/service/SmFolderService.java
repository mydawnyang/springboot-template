package com.yxh.www.template.service;

import com.yxh.www.common.base.BaseService;
import com.yxh.www.template.domain.SmFolder;

/**
 * <p>
 * 系统文件夹表 服务类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmFolderService extends BaseService<SmFolder> {

}
