package com.yxh.www.template.service;

import com.yxh.www.common.base.BaseService;
import com.yxh.www.template.domain.SmMailSender;

/**
 * <p>
 * 邮件发送方表 服务类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-12-11
 */
public interface SmMailSenderService extends BaseService<SmMailSender> {

}
