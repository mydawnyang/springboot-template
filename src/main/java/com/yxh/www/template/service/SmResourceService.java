package com.yxh.www.template.service;

import com.yxh.www.common.base.BaseService;
import com.yxh.www.common.result.Result;
import com.yxh.www.template.domain.SmResource;
import com.yxh.www.template.dto.SmResourceDTO;
import com.yxh.www.template.dto.VueMenu;

import java.util.List;

/**
 * <p>
 * 系统资源表 服务类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmResourceService extends BaseService<SmResource> {
    /**
     * 增加系统资源（菜单）信息
     *
     * @param resName  资源名称 Title
     * @param resRoute 资源路由
     * @param resLogo  资源Logo
     * @param resId    资源标识
     * @param resType  资源类型0：菜单 1：按钮
     * @param parentId 父级资源ID
     * @param remark   备注
     * @return 响应体
     */
    Result addSmResource(String resName,
                         String resRoute,
                         String resLogo,
                         String resId,
                         Integer resType,
                         String parentId,
                         String remark);

    /**
     * 修改资源信息
     *
     * @param smResourceId 资源ID
     * @param resName      资源名称 Title
     * @param resRoute     资源路由
     * @param resLogo      资源Logo
     * @param resId        资源标识
     * @param resType      资源类型0：菜单 1：按钮
     * @param resStatus    资源状态0：正常；1：失效
     * @param parentId     父级资源ID
     * @param remark       备注
     * @return 响应体
     */
    Result modifySmResource(String smResourceId,
                            String resName,
                            String resRoute,
                            String resLogo,
                            String resId,
                            Integer resType,
                            Integer resStatus,
                            String parentId,
                            String remark);

    /**
     * 删除系统资源
     *
     * @param smResourceId 资源ID
     * @return 响应体
     */
    Result removeSmResource(String smResourceId);
    /**
     * 删除多个系统资源
     *
     * @param smResourceIds 资源ID集合
     * @return 响应体
     */
    Result removeSmResources(String[] smResourceIds);

    /**
     * 获取角色变配资源 已拥有的资源和未分配的资源树结构列表
     * @param smRoleId 角色ID
     * @return 响应体
     */
    Result treeSmRoleSmResourceRelation(String smRoleId);
    /**
     * 查询所有资源，树形结构返回
     * @return 树形所有资源
     */
    Result treeAllSmResource();

    /**
     * 根据用户查询用户拥有的资源，树形结构返回
     * @param smUserId 用户ID
     * @return 资源列表
     */
    Result treeSmResourceByUserId(String smUserId);

    /**
     * 获取用户权限资源信息
     * @param smUserId  用户ID
     * @return  权限资源列表
     */
    List<VueMenu> treeVueMenuSmResourceByUserId(String smUserId);

    /**
     * 根据角色查询角色拥有的资源
     * @param smRoleId 角色ID
     * @return 资源列表
     */
    Result treeSmResourceByRoleId(String smRoleId);

    /**
     * 根据角色查询角色所有资源ID
     * @param smRoleId  角色ID
     * @return  资源ID集合
     */
    Result listSmResourceId(String smRoleId);

    /**
     * 转换资源结果方法
     * @param transform 所有待转换资源列表
     * @param result 结果
     */
    void transformSmResourceDTO(List<SmResource> transform, List<SmResourceDTO> result);

    /**
     * 转换资源SmResource到VueMenu并递归构建子节点
     * @param transform 需要转换的资源列表
     * @param result    接收结果
     */
    void transformVueMenu(List<SmResource> transform, List<VueMenu> result);

    /**
     * 获取子级资源列表
     * @param treeSmResource  根节点列表
     * @return  SmResourceDTO列表
     */
    void listSmResourceChildren(List<SmResourceDTO> treeSmResource);

}
