package com.yxh.www.template.service;

import com.yxh.www.common.base.BaseService;
import com.yxh.www.common.result.Result;
import com.yxh.www.template.domain.SmRoleResourceRelation;

/**
 * <p>
 * 系统角色资源关系表 服务类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmRoleResourceRelationService extends BaseService<SmRoleResourceRelation> {
    /**
     * 新增角色关联资源关系
     *
     * @param roleId     角色ID
     * @param resourceId 资源ID
     * @return 响应体
     */
    Result addSmRoleResourceRelation(String roleId, String resourceId);

    /**
     * 新增多个角色关联资源关系
     * @param roleId 角色ID
     * @param resourceIds 资源ID
     * @param requestUserId    用户ID
     * @return 响应体
     */
    Result addSmRoleResourcesRelation(String roleId, String[] resourceIds, String requestUserId);

    /**
     * 修改更新角色资源信息
     * @param roleId    角色ID
     * @param resourceIds    资源ID
     * @param requestUserId    用户ID
     * @return  响应体
     */
    Result modifySmRoleResourcesRelation(String roleId, String[] resourceIds, String requestUserId);

    /**
     * 删除角色资源关系
     *
     * @param smRoleResourceRelationId 角色资源关系ID
     * @return 响应体
     */
    Result removeSmRoleResourceRelation(String smRoleResourceRelationId);

    /**
     * 删除多个角色资源关系
     * @param roleId    角色ID
     * @param resourceIds   资源ID集合
     * @return 响应体
     */
    Result removeSmRoleResourcesRelation(String roleId, String[] resourceIds);
}
