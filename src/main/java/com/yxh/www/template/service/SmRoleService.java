package com.yxh.www.template.service;

import com.yxh.www.common.base.BaseService;
import com.yxh.www.common.result.Result;
import com.yxh.www.template.domain.SmRole;

/**
 * <p>
 * 系统角色表 服务类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmRoleService extends BaseService<SmRole> {
    /**
     * 新增系统角色
     *
     * @param roleName      角色名称
     * @param remark        角色备注
     * @param requestUserId 请求用户ID
     * @return 响应体
     */
    Result addSmRole(String roleName, String remark, String requestUserId);

    /**
     * 删除角色
     *
     * @param smRoleId 角色ID
     * @return 响应体
     */
    Result removeSmRole(String smRoleId);

    /**
     * 删除多个系统角色
     * @param smRoleIds 多个系统角色
     * @return  响应体
     */
    Result removeSmRoles(String[] smRoleIds);

    /**
     * 修改角色
     * @param smRoleId  角色ID
     * @param roleName  角色名称
     * @param remark    角色备注
     * @return  响应体
     */
    Result modifySmRole(String smRoleId, String roleName, String remark);

    /**
     * 查询所有角色列表
     * @return 角色列表
     */
    Result listAllSmRoles();
}
