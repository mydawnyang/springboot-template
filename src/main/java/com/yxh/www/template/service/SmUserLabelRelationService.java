package com.yxh.www.template.service;

import com.yxh.www.common.base.BaseService;
import com.yxh.www.template.domain.SmUserLabelRelation;

/**
 * <p>
 * 用户标签关系表 服务类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmUserLabelRelationService extends BaseService<SmUserLabelRelation> {

}
