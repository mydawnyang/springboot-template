package com.yxh.www.template.service;

import com.yxh.www.common.base.BaseService;
import com.yxh.www.template.domain.SmUserLabel;

/**
 * <p>
 * 用户标签表 服务类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmUserLabelService extends BaseService<SmUserLabel> {

}
