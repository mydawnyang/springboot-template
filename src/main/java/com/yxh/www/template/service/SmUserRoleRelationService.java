package com.yxh.www.template.service;

import com.yxh.www.common.base.BaseService;
import com.yxh.www.common.result.Result;
import com.yxh.www.template.domain.SmUserRoleRelation;

/**
 * <p>
 * 系统用户角色关系表 服务类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmUserRoleRelationService extends BaseService<SmUserRoleRelation> {
    /**
     * 新增用户角色关系信息
     * @param userId    用户id
     * @param roleId    角色id
     * @return  响应体
     */
    Result addSmUserRoleRelation(String userId, String roleId);

    /**
     * 删除用户角色关系
     * @param smUserRoleRelationId  用户角色关系ID
     * @return  响应体
     */
    Result removeSmUserRoleRelation(String smUserRoleRelationId);

    /**
     * 修改用户角色列表
     * @param smUserId 用户id
     * @param roleIds   角色ID 集合
     * @param requestUserId 请求用户
     * @return  响应体
     */
    Result modifySmUserRoleRelation(String smUserId, String[] roleIds, String requestUserId);
}
