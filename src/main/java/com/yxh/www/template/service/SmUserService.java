package com.yxh.www.template.service;

import com.yxh.www.common.base.BaseService;
import com.yxh.www.common.result.Result;
import com.yxh.www.template.domain.SmUser;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
public interface SmUserService extends BaseService<SmUser> {
    /**
     * 增加系统用户
     * @param userPhoto 用户头像
     * @param userName  用户名称
     * @param userPhone 用户手机号
     * @param loginPass 登录密码 默认 123456789
     * @param userEmail 用户邮箱
     * @return  响应体
     */
    Result addSmUser(String userPhoto,
                     String userName,
                     String userPhone,
                     String loginPass,
                     String userEmail);

    /**
     * 修改系统用户
     * @param smUserId 用户ID
     * @param userPhoto 用户头像
     * @param userName  用户名称
     * @param userPhone 用户手机号
     * @param loginPass 登录密码 默认 123456789
     * @param userEmail 用户邮箱
     * @param userStatus 用户状态
     * @return 响应体
     */
    Result modifySmUser(String smUserId,
                        String userPhoto,
                        String userName,
                        String userPhone,
                        String loginPass,
                        String userEmail,
                        Integer userStatus);

    /**
     * 分页查询系统用户列表
     * @param userName  用户名称
     * @param userPhone 用户手机号
     * @param userEmail 用户邮箱
     * @param pageNum   页码
     * @param pageSize  页面数据长度
     * @return  系统用户列表
     */
    Result listPageSmUser(String userName,
                          String userPhone,
                          String userEmail,
                          Long pageNum,
                          Long pageSize);

    /**
     * 用户手机号登录
     * @param userPhone 手机号码
     * @param passWord  用户密码
     * @return  响应体
     */
    Result login(String userPhone, String passWord);

    /**
     * 根据ID删除系统用户
     * @param ids   id
     * @return 响应体
     */
    Result removeSmUsers(String... ids);
}
