package com.yxh.www.template.service.impl;

import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.template.domain.SmDataDictionary;
import com.yxh.www.template.mapper.SmDataDictionaryMapper;
import com.yxh.www.template.service.SmDataDictionaryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统数据字典表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Service
public class SmDataDictionaryServiceImpl extends BaseServiceImpl<SmDataDictionaryMapper, SmDataDictionary> implements SmDataDictionaryService {

}
