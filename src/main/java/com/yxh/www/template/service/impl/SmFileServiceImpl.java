package com.yxh.www.template.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.OSS;
import com.aliyun.oss.model.PutObjectResult;
import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.common.result.Result;
import com.yxh.www.common.result.ResultBuilder;
import com.yxh.www.common.result.ResultEnum;
import com.yxh.www.template.constant.FileOssBucketConstant;
import com.yxh.www.template.domain.SmFile;
import com.yxh.www.template.enums.FileOssFolderEnum;
import com.yxh.www.template.mapper.SmFileMapper;
import com.yxh.www.template.service.SmFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * <p>
 * 系统文件表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Slf4j
@Service
public class SmFileServiceImpl extends BaseServiceImpl<SmFileMapper, SmFile> implements SmFileService {
    @Autowired
    private OSS ossClient;

    @Override
    public Result uploadUserPhoto(MultipartFile userPhoto) {
        if (userPhoto == null || userPhoto.getSize() <= 0) {
            return ResultBuilder.error(ResultEnum.USER_HANDEL_ERROR, "请上传有效的文件！");
        }
        try {
            SmFile smFile = new SmFile(FileOssFolderEnum.USER_PHOTO_FOLDER, userPhoto);
            InputStream inputStream = userPhoto.getInputStream();
            PutObjectResult result = ossClient.putObject(FileOssBucketConstant.MOA, smFile.getPhysicsPath(), inputStream);
            log.info(JSONObject.toJSONString(result));
        } catch (IOException ioE) {
            log.error("图片流获取异常", ioE);
            return ResultBuilder.error(ResultEnum.USER_HANDEL_ERROR, "文件读取异常，请检查文件是否存在！");
        }
        return ResultBuilder.success();
    }

    @Override
    public Result uploadProjectPhoto(MultipartFile projectPhoto) {
        if (projectPhoto == null || projectPhoto.getSize() <= 0) {
            return ResultBuilder.error(ResultEnum.USER_HANDEL_ERROR, "请上传有效的文件！");
        }
        try {
            SmFile smFile = new SmFile(FileOssFolderEnum.PROJECT_PHOTO_FOLDER, projectPhoto);
            InputStream inputStream = projectPhoto.getInputStream();
            PutObjectResult result = ossClient.putObject(FileOssBucketConstant.MOA, smFile.getPhysicsPath(), inputStream);
            log.info(JSONObject.toJSONString(result));
        } catch (IOException ioE) {
            log.error("图片流获取异常", ioE);
            return ResultBuilder.error(ResultEnum.USER_HANDEL_ERROR, "文件读取异常，请检查文件是否存在！");
        }
        return ResultBuilder.success();
    }
}
