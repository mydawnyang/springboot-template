package com.yxh.www.template.service.impl;

import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.template.domain.SmFolder;
import com.yxh.www.template.mapper.SmFolderMapper;
import com.yxh.www.template.service.SmFolderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统文件夹表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Service
public class SmFolderServiceImpl extends BaseServiceImpl<SmFolderMapper, SmFolder> implements SmFolderService {

}
