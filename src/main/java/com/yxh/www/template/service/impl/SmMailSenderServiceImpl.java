package com.yxh.www.template.service.impl;

import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.template.domain.SmMailSender;
import com.yxh.www.template.mapper.SmMailSenderMapper;
import com.yxh.www.template.service.SmMailSenderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 邮件发送方表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-12-11
 */
@Service
public class SmMailSenderServiceImpl extends BaseServiceImpl<SmMailSenderMapper, SmMailSender> implements SmMailSenderService {

}
