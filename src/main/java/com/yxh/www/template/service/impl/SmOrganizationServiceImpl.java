package com.yxh.www.template.service.impl;

import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.template.domain.SmOrganization;
import com.yxh.www.template.mapper.SmOrganizationMapper;
import com.yxh.www.template.service.SmOrganizationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统组织表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Service
public class SmOrganizationServiceImpl extends BaseServiceImpl<SmOrganizationMapper, SmOrganization> implements SmOrganizationService {

}
