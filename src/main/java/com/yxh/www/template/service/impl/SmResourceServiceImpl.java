package com.yxh.www.template.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.common.result.Result;
import com.yxh.www.common.result.ResultBuilder;
import com.yxh.www.common.result.ResultEnum;
import com.yxh.www.template.domain.SmResource;
import com.yxh.www.template.dto.SmResourceDTO;
import com.yxh.www.template.dto.VueMenu;
import com.yxh.www.template.mapper.SmResourceMapper;
import com.yxh.www.template.service.SmResourceService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 系统资源表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Service
public class SmResourceServiceImpl extends BaseServiceImpl<SmResourceMapper, SmResource> implements SmResourceService {

    @Override
    public Result addSmResource(String resName, String resRoute, String resLogo, String resId, Integer resType, String parentId, String remark) {
        SmResource smResource = new SmResource(resName, resRoute, resLogo, resId, resType, parentId, remark);
        smResource.insert();
        return ResultBuilder.success();
    }

    @Override
    public Result modifySmResource(String smResourceId, String resName, String resRoute, String resLogo, String resId, Integer resType, Integer resStatus, String parentId, String remark) {
        SmResource smResource = new SmResource(smResourceId, resName, resRoute, resLogo, resId, resType, resStatus, parentId, remark);
        smResource.updateById();
        return ResultBuilder.success();
    }

    @Override
    public Result removeSmResource(String smResourceId) {
        baseMapper.deleteById(smResourceId);
        return ResultBuilder.success();
    }

    @Override
    public Result removeSmResources(String[] smResourceIds) {
        if (null == smResourceIds || smResourceIds.length <= 0) {
            return ResultBuilder.error(ResultEnum.USER_HANDEL_ERROR, "不正确的资源ID参数");
        }
        baseMapper.deleteBatchIds(Arrays.asList(smResourceIds));
        return ResultBuilder.success();
    }

    @Override
    public Result treeSmRoleSmResourceRelation(String smRoleId) {
        if (StringUtils.isBlank(smRoleId)) {
            return ResultBuilder.success();
        }
        // 所有已分配资源
        List<SmResource> allRoleSmResource = baseMapper.listResourceByRoleId(smRoleId);
        // 所有已分配资源ID
        List<String> allRoleSmResourceIds = allRoleSmResource.stream().map(SmResource::getId).collect(Collectors.toList());
        // 未分配资源根节点数据
        List<SmResource> allOtherSmResource = baseMapper.listResourceByNotInSmResourceIds(allRoleSmResourceIds);
        // 定义结果集接收集合
        List<SmResourceDTO> roleTreeResource = new ArrayList<>();
        List<SmResourceDTO> otherTreeResource = new ArrayList<>();
        // 获取角色已分配资源根节点
        allRoleSmResource.forEach(item -> {
            if (StringUtils.isBlank(item.getParentId())) {
                roleTreeResource.add(new SmResourceDTO(item));
            }
        });
        // 构造角色已分配资源子节点
        this.transformSmResourceDTO(allRoleSmResource, roleTreeResource);
        // 获取其他未分配资源根节点
        allOtherSmResource.forEach(item -> {
            if (StringUtils.isBlank(item.getParentId())) {
                otherTreeResource.add(new SmResourceDTO(item));
            }
        });
        // 构造其他未分配资源子节点
        this.transformSmResourceDTO(allOtherSmResource, otherTreeResource);
        // 构造最后结果返回
        JSONObject result = new JSONObject() {{
            put("selectedSmResource", roleTreeResource);
            put("allSmResource", otherTreeResource);
        }};
        return ResultBuilder.success(result);
    }

    @Override
    public Result treeAllSmResource() {
        // 获取根节点数据
        List<SmResourceDTO> treeSmResource = baseMapper.listSmResourceChildren("");
        // 递归获取子节点数据
        this.listSmResourceChildren(treeSmResource);
        System.out.println(JSONObject.toJSONString(treeSmResource));
        return ResultBuilder.success(treeSmResource);
    }

    @Override
    public Result treeSmResourceByUserId(String smUserId) {
        List<SmResourceDTO> userTreeResource = new ArrayList<>();
        List<SmResource> allUserSmResource = baseMapper.listResourceByUserId(smUserId);
        // 获取根节点
        allUserSmResource.forEach(item -> {
            if (StringUtils.isBlank(item.getParentId())) {
                userTreeResource.add(new SmResourceDTO(item));
            }
        });
        // 构造子节点
        this.transformSmResourceDTO(allUserSmResource, userTreeResource);
        return ResultBuilder.success(userTreeResource);
    }

    @Override
    public List<VueMenu> treeVueMenuSmResourceByUserId(String smUserId) {
        List<VueMenu> userTreeResource = new ArrayList<>();
        List<SmResource> allUserSmResource = baseMapper.listResourceByUserId(smUserId);
        // 获取根节点
        allUserSmResource.forEach(item -> {
            if (StringUtils.isBlank(item.getParentId())) {
                userTreeResource.add(new VueMenu(item));
            }
        });
        // 构造子节点
        this.transformVueMenu(allUserSmResource, userTreeResource);
        return userTreeResource;
    }

    @Override
    public Result treeSmResourceByRoleId(String smRoleId) {
        if (StringUtils.isBlank(smRoleId)) {
            return ResultBuilder.success();
        }
        List<SmResourceDTO> roleTreeResource = new ArrayList<>();
        List<SmResource> allRoleSmResource = baseMapper.listResourceByRoleId(smRoleId);
        // 获取根节点
        allRoleSmResource.forEach(item -> {
            if (StringUtils.isBlank(item.getParentId())) {
                roleTreeResource.add(new SmResourceDTO(item));
            }
        });
        // 构造子节点
        this.transformSmResourceDTO(allRoleSmResource, roleTreeResource);
        return ResultBuilder.success(roleTreeResource);
    }

    @Override
    public Result listSmResourceId(String smRoleId) {
        if (StringUtils.isBlank(smRoleId)) {
            return ResultBuilder.success();
        }
        List<SmResource> allRoleSmResource = baseMapper.listResourceByRoleId(smRoleId);
        return ResultBuilder.success(allRoleSmResource.stream().map(SmResource::getId).collect(Collectors.toList()));
    }

    @Override
    public void transformSmResourceDTO(List<SmResource> transform, List<SmResourceDTO> result) {
        if (null == transform || transform.size() == 0) {
            return;
        }
        for (SmResourceDTO smResourceDTO : result) {
            List<SmResourceDTO> children = transform.stream().filter(smResource -> smResourceDTO.getId().equals(smResource.getParentId() == null ? "" : smResource.getParentId())).map(smResource -> new SmResourceDTO(smResource)).collect(Collectors.toList());
            if (children.size() > 0) {
                transformSmResourceDTO(transform, children);
                smResourceDTO.setChildren(children);
            }
        }
    }

    @Override
    public void transformVueMenu(List<SmResource> transform, List<VueMenu> result) {
        if (null == transform || transform.size() == 0) {
            return;
        }
        for (VueMenu vueMenu : result) {
            List<VueMenu> children = transform.stream().filter(smResource -> vueMenu.getId().equals(smResource.getParentId() == null ? "" : smResource.getParentId())).map(smResource -> new VueMenu(smResource)).collect(Collectors.toList());
            if (children.size() > 0) {
                transformVueMenu(transform, children);
                vueMenu.setChildren(children);
            }
        }
    }
    @Override
    public void listSmResourceChildren(List<SmResourceDTO> treeSmResource) {
        for (SmResourceDTO parentResource : treeSmResource) {
            List<SmResourceDTO> children = baseMapper.listSmResourceChildren(parentResource.getId());
            if (null != children && children.size() > 0) {
                listSmResourceChildren(children);
                // 设置子节点数据
                parentResource.setChildren(children);
            }
        }
    }

}
