package com.yxh.www.template.service.impl;

import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.common.result.Result;
import com.yxh.www.common.result.ResultBuilder;
import com.yxh.www.template.domain.SmRoleResourceRelation;
import com.yxh.www.template.mapper.SmRoleResourceRelationMapper;
import com.yxh.www.template.service.SmRoleResourceRelationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashMap;

/**
 * <p>
 * 系统角色资源关系表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Service
@Transactional
public class SmRoleResourceRelationServiceImpl extends BaseServiceImpl<SmRoleResourceRelationMapper, SmRoleResourceRelation> implements SmRoleResourceRelationService {

    @Override
    public Result addSmRoleResourceRelation(String roleId, String resourceId) {
        SmRoleResourceRelation smRoleResourceRelation=new SmRoleResourceRelation(roleId,resourceId);
        smRoleResourceRelation.setCreateTime(LocalDateTime.now());
        smRoleResourceRelation.insert();
        return ResultBuilder.success();
    }

    @Override
    public Result addSmRoleResourcesRelation(String roleId, String[] resourceIds, String requestUserId) {
        // 增加选中的角色资源
        if (resourceIds!=null){
            LocalDateTime now=LocalDateTime.now();
            for (String resourceId:resourceIds){
                new SmRoleResourceRelation(roleId,resourceId,requestUserId,now).insertOrUpdate();
            }
        }
        return ResultBuilder.success();
    }

    @Override
    public Result modifySmRoleResourcesRelation(String roleId, String[] resourceIds, String requestUserId) {
        // 删除角色资源
        baseMapper.deleteByMap(new HashMap<String, Object>(1){{put("role_id",roleId);}});
        // 增加选中的角色资源
        LocalDateTime now=LocalDateTime.now();
        if (resourceIds!=null){
            for (String resourceId:resourceIds){
                new SmRoleResourceRelation(roleId,resourceId,requestUserId,now).insert();
            }
        }
        return ResultBuilder.success();
    }

    @Override
    public Result removeSmRoleResourceRelation(String smRoleResourceRelationId) {
        baseMapper.deleteById(smRoleResourceRelationId);
        return ResultBuilder.success();
    }

    @Override
    public Result removeSmRoleResourcesRelation(String roleId, String[] resourceIds) {
        // 增加选中的角色资源
        if (resourceIds!=null){
            LocalDateTime now=LocalDateTime.now();
            for (String resourceId:resourceIds){
                SmRoleResourceRelation smRoleResourceRelation=new SmRoleResourceRelation(roleId,resourceId);
                baseMapper.deleteByMap(new HashMap<String, Object>(1){{
                    put("resource_id",resourceId);
                    put("role_id",roleId);
                }});
            }
        }
        return ResultBuilder.success();
    }
}
