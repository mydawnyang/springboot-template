package com.yxh.www.template.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.common.result.Result;
import com.yxh.www.common.result.ResultBuilder;
import com.yxh.www.common.result.ResultEnum;
import com.yxh.www.template.domain.SmRole;
import com.yxh.www.template.mapper.SmRoleMapper;
import com.yxh.www.template.mapper.SmRoleResourceRelationMapper;
import com.yxh.www.template.mapper.SmUserRoleRelationMapper;
import com.yxh.www.template.service.SmRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;

/**
 * <p>
 * 系统角色表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Service
@Transactional
public class SmRoleServiceImpl extends BaseServiceImpl<SmRoleMapper, SmRole> implements SmRoleService {
    @Autowired
    private SmUserRoleRelationMapper userRoleRelationMapper;
    @Autowired
    private SmRoleResourceRelationMapper roleResourceRelationMapper;

    @Override
    public Result addSmRole(String roleName, String remark, String requestUserId) {
        SmRole smRole=new SmRole(roleName,remark,requestUserId);
        smRole.setCreateTime(LocalDateTime.now());
        smRole.insert();
        return ResultBuilder.success();
    }

    @Override
    public Result removeSmRole(String smRoleId) {
        // 删除角色信息
        baseMapper.deleteById(smRoleId);
        // 删除角色资源关系
        roleResourceRelationMapper.deleteByMap(new HashMap<String, Object>(){{put("role_id",smRoleId);}});
        // 删除用户角色关系
        userRoleRelationMapper.deleteByMap(new HashMap<String, Object>(){{put("role_id",smRoleId);}});
        return ResultBuilder.success();
    }

    @Override
    public Result removeSmRoles(String[] smRoleIds) {
        if (null==smRoleIds||smRoleIds.length<=0){
            return ResultBuilder.error(ResultEnum.USER_HANDEL_ERROR,"请选择正确的角色ID！");
        }else {
            baseMapper.deleteBatchIds(Arrays.asList(smRoleIds));
        }
        return ResultBuilder.success();
    }

    @Override
    public Result modifySmRole(String smRoleId, String roleName, String remark) {
        SmRole smRole=new SmRole();
        smRole.setId(smRoleId);
        smRole.setRoleName(roleName);
        smRole.setRemark(remark);
        smRole.updateById();
        return ResultBuilder.success();
    }

    @Override
    public Result listAllSmRoles() {
        return ResultBuilder.success(baseMapper.selectList(new QueryWrapper<>()));
    }

}
