package com.yxh.www.template.service.impl;

import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.template.domain.SmUserLabelRelation;
import com.yxh.www.template.mapper.SmUserLabelRelationMapper;
import com.yxh.www.template.service.SmUserLabelRelationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户标签关系表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Service
public class SmUserLabelRelationServiceImpl extends BaseServiceImpl<SmUserLabelRelationMapper, SmUserLabelRelation> implements SmUserLabelRelationService {

}
