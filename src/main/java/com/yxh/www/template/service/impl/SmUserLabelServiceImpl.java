package com.yxh.www.template.service.impl;

import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.template.domain.SmUserLabel;
import com.yxh.www.template.mapper.SmUserLabelMapper;
import com.yxh.www.template.service.SmUserLabelService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户标签表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Service
public class SmUserLabelServiceImpl extends BaseServiceImpl<SmUserLabelMapper, SmUserLabel> implements SmUserLabelService {

}
