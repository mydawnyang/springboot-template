package com.yxh.www.template.service.impl;

import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.common.result.Result;
import com.yxh.www.common.result.ResultBuilder;
import com.yxh.www.template.domain.SmUserRoleRelation;
import com.yxh.www.template.mapper.SmUserRoleRelationMapper;
import com.yxh.www.template.service.SmUserRoleRelationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashMap;

/**
 * <p>
 * 系统用户角色关系表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Service
@Transactional
public class SmUserRoleRelationServiceImpl extends BaseServiceImpl<SmUserRoleRelationMapper, SmUserRoleRelation> implements SmUserRoleRelationService {

    @Override
    public Result addSmUserRoleRelation(String userId, String roleId) {
        SmUserRoleRelation smUserRoleRelation=new SmUserRoleRelation(userId,roleId);
        smUserRoleRelation.setCreateTime(LocalDateTime.now());
        smUserRoleRelation.insert();
        return ResultBuilder.success();
    }

    @Override
    public Result removeSmUserRoleRelation(String smUserRoleRelationId) {
        baseMapper.deleteById(smUserRoleRelationId);
        return ResultBuilder.success();
    }

    @Override
    public Result modifySmUserRoleRelation(String smUserId, String[] roleIds, String requestUserId) {
        // 删除该用户角色列表
        baseMapper.deleteByMap(new HashMap<String, Object>(1){{put("user_id",smUserId);}});
        // 初始化当前时间
        LocalDateTime now=LocalDateTime.now();
        // 判断是否有需要绑定的角色
        if (null==roleIds||roleIds.length==0){
            return ResultBuilder.success();
        }
        // 新增该用户角色列表
        for (String roleId:roleIds){
            new SmUserRoleRelation(smUserId,roleId,requestUserId,now).insertOrUpdate();
        }
        return ResultBuilder.success();
    }
}
