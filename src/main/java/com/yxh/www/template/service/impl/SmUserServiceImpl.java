package com.yxh.www.template.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yxh.www.common.base.BaseServiceImpl;
import com.yxh.www.common.result.Result;
import com.yxh.www.common.result.ResultBuilder;
import com.yxh.www.common.result.ResultEnum;
import com.yxh.www.template.domain.SmUser;
import com.yxh.www.template.dto.SmUserDTO;
import com.yxh.www.template.dto.TokenUser;
import com.yxh.www.template.dto.VueMenu;
import com.yxh.www.template.mapper.SmUserMapper;
import com.yxh.www.template.service.SmResourceService;
import com.yxh.www.template.service.SmUserService;
import com.yxh.www.template.util.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 系统用户表 服务实现类
 * </p>
 *
 * @author Yangxiaohui
 * @since 2019-11-26
 */
@Slf4j
@Service
public class SmUserServiceImpl extends BaseServiceImpl<SmUserMapper, SmUser> implements SmUserService {
    private SmResourceService smResourceService;

    public SmUserServiceImpl(SmResourceService smResourceService) {
        this.smResourceService = smResourceService;
    }

    @Override
    public Result addSmUser(String userPhoto, String userName, String userPhone, String loginPass, String userEmail) {
        // 加密密码 MD5加密用户 手机号+密码
        String md5Pass = DigestUtils.md5Hex(userPhone + loginPass);
        SmUser smUser = new SmUser(userPhoto, userName, userPhone, md5Pass, userEmail);
        try {
            smUser.setCreateTime(LocalDateTime.now());
            smUser.insert();
        } catch (Exception e) {
            log.info("增加系统用户失败：", e);
            return ResultBuilder.error(ResultEnum.USER_HANDEL_ERROR, "用户新增失败~请检查手机号是否错误或重复!");
        }
        return ResultBuilder.success();
    }

    @Override
    public Result modifySmUser(String smUserId, String userPhoto, String userName, String userPhone, String loginPass, String userEmail, Integer userStatus) {
        SmUser smUser = new SmUser(smUserId, userPhoto, userName, userPhone, loginPass, userEmail,userStatus);
        try {
            smUser.updateById();
        } catch (Exception e) {
            log.info("修改系统用户失败：", e);
            return ResultBuilder.error(ResultEnum.USER_HANDEL_ERROR, "用户修改失败请检查手机号是否错误或重复!");
        }
        return ResultBuilder.success();
    }

    @Override
    public Result listPageSmUser(String userName, String userPhone, String userEmail, Long pageNum, Long pageSize) {
        IPage<SmUserDTO> page = baseMapper.listPageSmUser(new Page<>(pageNum,pageSize),userName, userPhone, userEmail);
        return ResultBuilder.success(page);
    }

    @Override
    public Result login(String userPhone, String passWord) {
        String md5Pass = DigestUtils.md5Hex(userPhone + passWord);
        if (baseMapper.checkUserPass(userPhone, md5Pass) == 1) {
            SmUser user = baseMapper.selectOne(new QueryWrapper<SmUser>().eq("user_phone", userPhone));
            // 更新用户最后登录时间
            user.setLastLoginTime(LocalDateTime.now());
            user.updateById();
            // 获取用户资源
            List<VueMenu> menus = smResourceService.treeVueMenuSmResourceByUserId(user.getId());
            // 创建TokenUser
            TokenUser tokenUser = new TokenUser(user,menus);
            try {
                // 构造Token
                String token=JwtTokenUtil.buildUserLoginToken(tokenUser);
                tokenUser.setToken(token);
                // 用来判断用户信息是否发生了变动
                String md5KeyStr=tokenUser.getUserPhone()+tokenUser.getLoginPass()+JSONObject.toJSONString(menus);
                tokenUser.setMd5Key(DigestUtils.md5Hex(md5KeyStr));
                return ResultBuilder.success(tokenUser);
            }catch (Exception e){
                log.info("Token生成失败：",e);
                return ResultBuilder.error(ResultEnum.USER_LOGIN_ERROR, "系统忙.请稍后重试！");
            }
        }
        return ResultBuilder.error(ResultEnum.USER_LOGIN_ERROR, "无效的用户或密码！");
    }

    @Override
    public Result removeSmUsers(String... ids) {
        if (null!=ids&&ids.length!=0) {
            baseMapper.deleteBatchIds(Arrays.asList(ids));
        }
        return ResultBuilder.success();
    }

}
