package com.yxh.www.template.util;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yangxiaohui
 * @date Create by 2019/12/26 3:21 下午
 */
public class ParameterServletRequestWrapper extends HttpServletRequestWrapper {
    private Map<String, String[]> params = new ConcurrentHashMap<>();

    public ParameterServletRequestWrapper(HttpServletRequest request) {
        super(request);
        this.params.putAll(request.getParameterMap());
    }

    /**
     * 重载一个构造方法
     *
     * @param request      请求
     * @param extendParams 参数
     */
    public ParameterServletRequestWrapper(HttpServletRequest request, Map<String, String[]> extendParams) throws IOException {
        this(request);
        addAllParameters(request.getMethod(), extendParams);
    }

    @Override
    public String getParameter(String name) {
        String[] values = params.get(name);
        if (values == null || values.length == 0) {
            return null;
        }
        return values[0];
    }

    @Override
    public String[] getParameterValues(String name) {
        return params.get(name);
    }

    public void addAllParameters(String methodType, Map<String, String[]> otherParams) {
        for (Map.Entry<String, String[]> entry : otherParams.entrySet()) {
            // 如果不是PUT请求修改数据时，数据传入"" 代表没有传参
            if (!"PUT".equals(methodType.toUpperCase())) {
                if (entry.getValue() != null && entry.getValue().length > 0 && StringUtils.isNotBlank(entry.getValue()[0])) {
                    addParameter(entry.getKey(), entry.getValue());
                }
            } else {
                addParameter(entry.getKey(), entry.getValue());
            }
        }
    }


    public void addParameter(String name, Object value) {
        if (value != null) {
            if (value instanceof String[]) {
                params.put(name, (String[]) value);
            } else if (value instanceof String) {
                params.put(name, new String[]{(String) value});
            } else {
                params.put(name, new String[]{String.valueOf(value)});
            }
        }
    }

}
